@extends('layouts.admin')

@section('posicion_url')
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Panel Administracion <small>Estadisticas Mensuales</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> Inicio
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
                <!--
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>Like SB Admin?</strong> Try out <a href="http://startbootstrap.com/template-overviews/sb-admin-2" class="alert-link">SB Admin 2</a> for additional features!
                        </div>
                    </div>
                </div>
                -->
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-comments fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">{{'10'}}</div>
                                        <div>Registros Recorridos</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left">Diarios</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-tasks fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"></div>
                                        <div> MES <strong>{{date('m')}}</strong> En Curso</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>


                </div>


                <!-- /.row -->

                <!-- 
                <div class="row">
                    <div class="col-md-4 col-lg-4">
                        <div class="form-group">
                            <label>Mes Reportes</label>
                            <select class="form-control">
                                <option>{{'Enero'}}</option>
                                <option>{{'Febrero'}}</option>
                                <option>{{'Marzo'}}</option>
                                <option>{{'Abril'}}</option>
                                <option>{{'Mayo'}}</option>
                                <option>{{'Junio'}}</option>
                                <option>{{'Julio'}}</option>
                                <option>{{'Agosto'}}</option>
                                <option>{{'Septiembre'}}</option>
                                <option>{{'Octubre'}}</option>
                                <option>{{'Noviembre'}}</option>
                                <option>{{'Diciembre'}}</option>

                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <div class="form-group">
                            <label>Año</label>
                            <select class="form-control">
                                @for($i =2013 ; $i < 2030 ; $i++)
                                <option
                                @if($i == date('Y'))
                                    selected
                                @endif
                                >{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                </div>
                <!-- 


                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i> Grafica de Reportes</h3>
                            </div>
                            <div class="panel-body">
                                <div id="morris-area-chart"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->



@endsection


@section('javascriptInc')
<!-- Morris Charts JavaScript -->
    <script src="{{asset('js/plugins/morris/raphael.min.js')}}"></script>
    <script src="{{asset('js/plugins/morris/morris.min.js')}}"></script>
    <script src="{{asset('js/plugins/morris/morris-data.js')}}"></script>

@endsection