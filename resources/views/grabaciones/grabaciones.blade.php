@extends('layouts.admin')

@section('posicion_url')
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Grabaciones <small> Panel :: Buscar </small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> Reportes
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')


    <div class="row">
        <?php
        echo Form::open(array('url' => asset('/grabaciones'),'files'=>'true'));
        ?>
        <div class="col-lg-4">
            <div class="form-group" name="campania">
                <label>Tipo Busqueda</label>
                <select class="form-control" name="tipoBusqueda">

                        <option value="1">Numero Propuesta</option>
                        <option value="2">Numero Telefono</option>

                </select>
            </div>
        </div>

        <div class="col-lg-4 ">
            <div class="form-group">
                <?php

                echo '<label>INGRESE BUSQUEDA</label>';
                echo Form::text('grabacion','',array('class'=>'form-control','required' , 'placeholder'=>''));
                ?>
                <br>
                <?php
                echo Form::submit('BUSCAR GRABACIONES',array('class'=>'btn btn btn-success'));

                ?>
            </div>
        </div>


            <div class="col-lg-4">

                <div class="col-md-4"><label>Mes</label>
                    <select class="form-control" name="mes">
                        <option value="01"
                                @if(1 == date('m'))
                                selected
                                @endif
                        >Enero</option>
                        <option value="02"
                                @if(2 == date('m'))
                                selected
                                @endif
                        >Febrero</option>
                        <option value="03"
                                @if(3 == date('m'))
                                selected
                                @endif
                        >Marzo</option>
                        <option value="04"
                                @if(4 == date('m'))
                                selected
                                @endif
                        >Abril</option>
                        <option value="05"
                                @if(5 == date('m'))
                                selected
                                @endif
                        >Mayo</option>
                        <option value="06"
                                @if(6 == date('m'))
                                selected
                                @endif
                        >Junio</option>
                        <option value="07"
                                @if(7 == date('m'))
                                selected
                                @endif
                        >Julio</option>
                        <option value="08"
                                @if(8 == date('m'))
                                selected
                                @endif
                        >Agosto</option>
                        <option value="09"
                                @if(9 == date('m'))
                                selected
                                @endif
                        >Septiembre</option>
                        <option value="10"
                                @if(10 == date('m'))
                                selected
                                @endif
                        >Octubre</option>
                        <option value="11"
                                @if(11 == date('m'))
                                selected
                                @endif
                        >Noviembre</option>
                        <option value="12"
                                @if(12 == date('m'))
                                selected
                                @endif
                        >Diciembre</option>
                    </select>
                </div>
                <div class="col-md-4"><label>Año</label>
                    <?php
                    $nuevafecha = strtotime ( '+5 year' , strtotime ( date('Y') ) ) ;
                    $nuevafecha = date('Y',$nuevafecha);
                    ?>
                    <select class="form-control" name="anio">
                        @for($i=2010; $i <= $nuevafecha ; $i++)
                            <option value="{{$i}}"
                                    @if($i == date('Y'))
                                    selected
                                    @endif
                            >{{$i}}</option>
                        @endfor
                    </select>
                </div>
            </div>

        <?php
        echo Form::close();
        ?>
    </div>


    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8 col-centered">
            <h2>Grabaciones</h2>
            <div class="table-responsive" style="background-color: white;">
                <table class="table table-bordered table-hover table-striped" >
                    <thead>
                    <tr>

                        <th>Mes</th>
                        <th>Día</th>
                        <th>Grabación</th>
                        <th>Descarga</th>

                    </tr>
                    </thead>
                    <tbody>

                    @foreach($grab as $grabaciones)
                        <tr>
                            <td>{{$grabaciones['mes']}}</td>
                            <td>{{$grabaciones['dia']}}</td>
                            <td>{{$grabaciones['grabacion']}}</td>
                            <td> <a href="{{asset("grabacion/".str_replace("#",'$',$grabaciones['grabacion'])) }}"  download><span class="glyphicon glyphicon-download"></span></a> </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-lg-2"></div>

@endsection


@section('javascriptInc')


@endsection
