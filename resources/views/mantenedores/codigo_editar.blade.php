@extends('layouts.admin')

@section('posicion_url')
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Codigo <small> Nuevo :: Usuarios </small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="{{route('home')}}" >Inicio </a> > Usuarios
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')

    <!-- /.row -->
    <div class="row">

        <div class="col-lg-4">
            <a href="{{asset('mantenedor/codigo/create')}}" class="btn btn-info" role="button">AGREGAR NUEVO</a>
        </div>

    </div>

    <br>

    <!-- /.row -->
    <form method="POST" action="{{ asset("mantenedor/codigo/") }} " enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-lg-3">

                <div class="form-group">
                    <label>Producto</label>

                    <input id="producto" type="text" class="form-control {{ $errors->has('producto') ? ' is-invalid' : '' }}" name="producto" value="{{ $codigo->producto }}" required autofocus>

                </div>

            </div>
            <div class="col-lg-3">

                <div class="form-group input-group">
                    <label>Codigo</label>
                    <input id="codigo" type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="codigo" required autofocus>
                </div>

            </div>

            <div class="col-lg-3">

                <div class="form-group input-group">
                    <label>Valor</label>
                    <input id="valor" type="text" class="form-control" name="valor" required autofocus>
                </div>
            </div>

            <div class="col-lg-3">

                <div class="form-group input-group">
                    <label>Estado</label>
                    <select class="form-control" name="estado">
                        <option value="1">ON</option>
                        <option value="0">OFF</option>
                    </select>
                </div>
            </div>

            <div class="col-lg-3">

                <div class="form-group input-group">
                    <label>Campaña</label>
                    <select class="form-control" name="campania">
                        @foreach($campania as $camp)
                            <option value="{{$camp->idCampania}}">{{$camp->nombre}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-lg-3">

                <div class="form-group input-group">
                    <label>Tipo</label>
                    <select class="form-control" name="tipo">
                        <option value="otro">Otro</option>
                        <option value="banco">Banco</option>
                        <option value="banefe">Banefe</option>
                    </select>
                </div>
            </div>


        </div>


        <div class="row">
            <!-- .row -->

            <div class="col-lg-12">
                <button type="submit" class="btn btn-primary">GUARDAR</button>
            </div>

            <!-- .row -->
        </div>
    </form>
    <!-- /.row -->
@endsection


@section('javascriptInc')
    <!-- Morris Charts JavaScript -->


@endsection