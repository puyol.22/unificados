@extends('layouts.admin')

@section('posicion_url')
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                CAMPAÑA <small> Nueva :: campaña  </small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="{{route('home')}}" >Inicio </a> > Campaña > agregar

                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')

    <!-- /.row -->

    <!-- /.row -->
    <form method="POST" action="{{ asset("mantenedor/campania/") }} " enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-lg-6">

                <div class="form-group">
                    <label>Nombre</label>

                    <input id="name" type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('nombre') }}" required autofocus>

                    @if ($errors->has('name'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif
                </div>

            </div>
            <div class="col-lg-6">

                <div class="form-group input-group">
                    <label>Tabla Link</label>
                    <select class="form-control" name="tabla">
                        <option value="propuesta">Propuestas</option>

                    </select>
                </div>

            </div>
        </div>
        <div class="row">
            <!-- .row -->
            <div class="col-lg-4">

                <div class="panel panel-green">
                    <div class="panel-heading">
                        <h3 class="panel-title">Variables para el Script</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered table-hover table-striped" style="font-size: 12px">
                            <thead>
                            <tr>
                                <th>Descripción</th>
                                <th>Variable</th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Nombre</td>
                                <td> Contratnte</td>
                            </tr>

                            <tr>
                                <td>Rut</td>
                                <td> RUT</td>
                            </tr>
                            <tr>
                                <td>Producto</td>
                                <td> Producto\\</td>
                            </tr>

                            <tr>
                                <td>Codigo Producto</td>
                                <td> Codigo_Producto</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

            <div class="col-lg-4">

                <div class="panel panel-green">
                    <div class="panel-heading">
                        <h3 class="panel-title">Variables para el Script</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered table-hover table-striped" style="font-size: 12px">
                            <thead>
                            <tr>
                                <th>Descripción</th>
                                <th>Variable</th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Nombre Ejecutivo</td>
                                <td>nombre_ejecutivo</td>
                            </tr>


                            <tr>
                                <td>Contratante</td>
                                <td> Contratante</td>
                            </tr>

                            <tr>
                                <td>Fecha Venta</td>
                                <td> Fecha_Venta</td>
                            </tr>
                            <tr>
                                <td>Asesor</td>
                                <td> Asesor_Corner</td>
                            </tr>

                            <tr>
                                <td>Plan</td>
                                <td>DscPlan</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>


            <div class="col-lg-4">

                <div class="panel panel-green">
                    <div class="panel-heading">
                        <h3 class="panel-title">Variables para el Script</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered table-hover table-striped" style="font-size: 12px">
                            <thead>
                            <tr>
                                <th>Descripción</th>
                                <th>Variable</th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Canal Venta</td>
                                <td>Canal_Venta</td>
                            </tr>

                            <tr>
                                <td>Rut Vendedor</td>
                                <td>Rut_Vendedor</td>
                            </tr>
                            <tr style="font-size: 11px">
                                <td>Sucursal Vendedor</td>
                                <td>Sucursal_Vendedor</td>
                            </tr>

                            <tr>
                                <td>Prima</td>
                                <td>prima</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

            <div class="col-lg-12">
                    <div class="form-group">
                        <label>Script</label>
                        <textarea class="form-control"  name="editor1" id="editor1" rows="50" cols="180"></textarea>
                    </div>
             </div>


            <br>
            <div class="col-lg-12">
                <button type="submit" class="btn btn-primary">GUARDAR</button>
            </div>

            <!-- .row -->
        </div>
    </form>

    <!-- /.row -->
@endsection


@section('javascriptInc')
    <!-- Morris Charts JavaScript -->
    <script src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script>
    <script>
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace( 'editor1' );
    </script>

@endsection