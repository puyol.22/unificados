@extends('layouts.admin')

@section('posicion_url')
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Tipo Usuario <small> Nuevo :: Tipo Usuario  </small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="{{route('home')}}" >Inicio </a> > Tipo Usuario > Agregar

                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')

    <!-- /.row -->

    <!-- /.row -->
    <form method="POST" action="{{ asset("mantenedor/tipousuario/") }} " enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-lg-6">

                <div class="form-group">
                    <label>Nombre</label>

                    <input id="name" type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('nombre') }}" required autofocus>

                    @if ($errors->has('name'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif
                </div>

            </div>
            <div class="col-lg-6">

                <div class="form-group input-group">
                    <label>Estado</label>
                    <select class="form-control" name="estado">
                        <option value="1">Activado</option>
                        <option value="2">Desctivado</option>

                    </select>
                </div>

            </div>
        </div>
        <div class="row">
            <!-- .row -->

            <div class="col-lg-12">
                <button type="submit" class="btn btn-primary">GUARDAR</button>
            </div>

            <!-- .row -->
        </div>
    </form>

    <!-- /.row -->
@endsection


@section('javascriptInc')
    <!-- Morris Charts JavaScript -->
    <script src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script>
    <script>
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace( 'editor1' );
    </script>

@endsection