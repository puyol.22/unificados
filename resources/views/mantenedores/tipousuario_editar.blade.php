@extends('layouts.admin')

@section('posicion_url')
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                TIPO USUARIO <small> Editar :: Tipo Usuario ({{$tipousuario->nombre}}) </small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="{{route('home')}}" >Inicio </a> > Tipo Usuario > Editar
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')

    <!-- /.row -->
    <div class="row">

        <div class="col-lg-4">
            <a href="{{asset('mantenedor/tipousuario/create')}}" class="btn btn-info" role="button">AGREGAR NUEVO</a>
        </div>

    </div>

    <br>

    <!-- /.row -->
    <form method="POST" action="{{ asset("mantenedor/tipousuario/$tipousuario->id") }}" enctype="multipart/form-data">
        <input name="_method" type="hidden" value="PUT">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-lg-6">

                <div class="form-group">
                    <label>Nombre</label>

                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $tipousuario->nombre?$tipousuario->nombre:old('nombre') }}" required autofocus>

                    @if ($errors->has('name'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>

            </div>

            <div class="col-lg-6">

                <div class="form-group input-group">
                    <label>Estado</label>
                    <select class="form-control" name="estado">
                        <option value="1"
                        @if($tipousuario->estado == 1)
                            selected
                            @endif
                        >Activado</option>
                        <option value="2"
                                @if($tipousuario->estado == 2)
                                selected
                                @endif
                        >Desctivado</option>

                    </select>
                </div>

            </div>
        </div>
        <div class="row">
            <!-- .row -->
            <div class="col-lg-12">
                <button type="submit" class="btn btn-primary">GUARDAR</button>
            </div>

            <!-- .row -->
        </div>
    </form>

    <!-- /.row -->
@endsection


@section('javascriptInc')
    <!-- Morris Charts JavaScript -->
    <script src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script>
    <script>
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace( 'editor1' );
    </script>

@endsection