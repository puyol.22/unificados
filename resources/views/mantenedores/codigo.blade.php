@extends('layouts.admin')

@section('posicion_url')
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Codigos <small> Mostrar :: Todos </small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> Codigos
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')

    <!-- /.row -->

    <!-- /.row -->
    <div class="row">

        <div class="col-lg-4">
            <a href="{{asset('mantenedor/codigo/create')}}" class="btn btn-info" role="button">AGREGAR NUEVO</a>
        </div>

    </div>
    <!-- /.row -->
    <br>
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6 col-centered">
            <h2>CODIGOS DE CAMPAÑAS</h2>
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>PRODUCTO</th>
                        <th>CODIGO</th>
                        <th>VALOR</th>
                        <th>CAMPAÑA</th>
                        <th>EDITAR</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($codigo as $co)
                        <tr>
                            <td>{{$co->id}}</td>
                            <td>{{$co->producto}}</td>
                            <td>{{$co->codigo}}</td>
                            <td>{{$co->valor}}</td>
                            <td>{{$co->idCampania}}</td>
                            <td> <a href="{{asset("mantenedor/codigo/$co->id/edit")}}"><span class="glyphicon glyphicon-pencil"></span></a> </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-lg-3"></div>

    </div>

    <!-- /.row -->



    <!-- /.row -->
@endsection


@section('javascriptInc')
    <!-- Morris Charts JavaScript -->
    <script src="{{asset('js/plugins/morris/raphael.min.js')}}"></script>
    <script src="{{asset('js/plugins/morris/morris.min.js')}}"></script>
    <script src="{{asset('js/plugins/morris/morris-data.js')}}"></script>

@endsection
