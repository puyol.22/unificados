@extends('layouts.admin')

@section('posicion_url')
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Campañas <small> Listar :: Campañas </small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="{{route('home')}}" >Inicio </a> > Campaña
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')

    <!-- /.row -->
    <div class="row">

        <div class="col-lg-4">
            <a href="{{asset("mantenedor/campania/create")}}" class="btn btn-info" role="button">AGREGAR NUEVO</a>
        </div>

    </div>
    <!-- /.row -->
    <br>
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6 col-centered">
            <h2>Camapaña</h2>
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>NOMBRE</th>
                        <th>EDITAR</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($campania as $camp)
                        <tr>
                            <td>{{$camp->idCampania}}</td>
                            <td>{{$camp->nombre}}</td>
                            <td> <a href="{{asset("mantenedor/campania/$camp->idCampania/edit")}}"><span class="glyphicon glyphicon-pencil"></span></a> </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-lg-3"></div>

    </div>

    <!-- /.row -->
@endsection


@section('javascriptInc')
    <!-- Morris Charts JavaScript -->


@endsection