@extends('layouts.admin')

@section('posicion_url')
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                CAMPAÑA <small> Editar :: campaña ({{$campania->nombre}}) </small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="{{route('home')}}" >Inicio </a> > Campaña > Editar
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')

    <!-- /.row -->
    <div class="row">

        <div class="col-lg-4">
            <a href="{{asset('mantenedor/campania/create')}}" class="btn btn-info" role="button">AGREGAR NUEVA</a>
        </div>

    </div>

    <br>

    <!-- /.row -->
    <form method="POST" action="{{ asset("mantenedor/campania/$campania->idCampania") }}" enctype="multipart/form-data">
        <input name="_method" type="hidden" value="PUT">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-lg-6">

                <div class="form-group">
                    <label>Nombre</label>

                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $campania->nombre?$campania->nombre:old('nombre') }}" required autofocus>

                    @if ($errors->has('name'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif
                </div>

            </div>
        </div>
        <div class="row">
            <!-- .row -->
            <div class="col-lg-4">

                <div class="panel panel-green">
                    <div class="panel-heading">
                        <h3 class="panel-title">Variables para el Script</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered table-hover table-striped" style="font-size: 12px">
                            <thead>
                            <tr>
                                <th>Descripción</th>
                                <th>Variable</th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Nombre Ejecutivo</td>
                                <td>nombre_ejecutivo</td>
                            </tr>


                            <tr>
                                <td>Nombre</td>
                                <td>Contratante</td>
                            </tr>


                            <tr>
                                <td>Rut</td>
                                <td>RUT</td>
                            </tr>
                            <tr>
                                <td>Producto</td>
                                <td>Producto</td>
                            </tr>

                            <tr>
                                <td>Codigo Producto</td>
                                <td>Codigo_Producto</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

            <div class="col-lg-4">

                <div class="panel panel-green">
                    <div class="panel-heading">
                        <h3 class="panel-title">Variables para el Script</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered table-hover table-striped" style="font-size: 12px">
                            <thead>
                            <tr>
                                <th>Descripción</th>
                                <th>Variable</th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Canal de Venta</td>
                                <td>Canal_Venta</td>
                            </tr>

                            <tr>
                                <td>Fecha Venta</td>
                                <td>Fecha_Venta</td>
                            </tr>
                            <tr>
                                <td>Asesor</td>
                                <td>Asesor_Corner</td>
                            </tr>

                            <tr>
                                <td>Plan</td>
                                <td>DscPlan</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>


            <div class="col-lg-4">

                <div class="panel panel-green">
                    <div class="panel-heading">
                        <h3 class="panel-title">Variables para el Script</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered table-hover table-striped" style="font-size: 12px">
                            <thead>
                            <tr>
                                <th>Descripción</th>
                                <th>Variable</th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Vendedor</td>
                                <td>Vendedor</td>
                            </tr>

                            <tr>
                                <td>Rut Vendedor</td>
                                <td>Rut_Vendedor</td>
                            </tr>
                            <tr style="font-size: 11px">
                                <td>Sucursal Vendedor</td>
                                <td>Sucursal_Vendedor</td>
                            </tr>

                            <tr>
                                <td>Prima</td>
                                <td>Prima</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

            <div class="col-lg-12">
                <div class="form-group">
                    <label>Script</label>
                    <textarea class="form-control"  name="editor1" id="editor1" rows="70" cols="360">{{$campania->script?$campania->script:old('editor')}}</textarea>
                </div>
            </div>


            <br>
            <div class="col-lg-12">
                <button type="submit" class="btn btn-primary">GUARDAR</button>
            </div>

            <!-- .row -->
        </div>
    </form>

    <!-- /.row -->
@endsection


@section('javascriptInc')
    <!-- Morris Charts JavaScript -->
    <script src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script>
    <script>
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace( 'editor1' );
    </script>

@endsection