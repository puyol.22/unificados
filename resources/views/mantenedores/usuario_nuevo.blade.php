@extends('layouts.admin')

@section('posicion_url')
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Usuarios <small> Listar :: Usuarios </small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="{{route('home')}}" >Inicio </a> > Usuarios
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')

    <!-- /.row -->
    <div class="row">

        <div class="col-lg-4">
            <a href="{{asset('mantenedor/usuario/create')}}" class="btn btn-info" role="button">AGREGAR NUEVO</a>
        </div>

    </div>

    <br>

    <!-- /.row -->
    <form method="POST" action="{{ route('register') }}">
        {{ csrf_field() }}
    <div class="row">
        <div class="col-lg-6">

            <div class="form-group">
                <label>Nombre</label>

                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                @if ($errors->has('name'))
                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="password" >Password</label>


                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif

            </div>

            <div class="form-group">
                <label for="password-confirm" >Confirm Password</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

            </div>


            <div class="form-group" name="tipousuario">
                <label>Tipo Usuario</label>
                <select class="form-control" name="tipousuario">
                    @foreach($tipousuario as $tus)
                        <option value="{{$tus->id}}"  >{{$tus->nombre}}</option>
                    @endforeach
                </select>
            </div>

        </div>

        <div class="col-lg-6">

            <div class="form-group">
                <label>Correo</label>
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif
            </div>


            <div class="form-group" name="campania">
                <label>Camapañas</label>
                <select class="form-control" name="campania">
                    @foreach($campania as $camp)
                    <option value="{{$camp->idCampania}}">{{$camp->nombre}}</option>
                    @endforeach
                </select>
            </div>

        </div>


        <br>
        <div class="col-lg-12">
            <button type="submit" class="btn btn-primary">GUARDAR</button>
        </div>

    </div>
    </form>
    <!-- /.row -->
@endsection


@section('javascriptInc')
    <!-- Morris Charts JavaScript -->


@endsection