<p>&nbsp;</p>

<p>Buenos días/tardes, ¿hablo con <strong>{{$propuesta->Contratante}}?</strong></p>

<p>&nbsp;</p>

<p>
Hola <strong>{{$propuesta->Contratante}}</strong> mi nombre es <strong>{{$propuesta->User->name}}</strong> y la estoy llamando de Santander Seguros ¿cómo
está? <br>
(pausa) <br>
Que bueno. El motivo de mi llamada es para darle la bienvenida por la contratación del seguro
<strong>{{$propuesta->Producto}}</strong> que realizó el día <strong>{{$propuesta->Fecha_Venta}}</strong> a través de <strong>{{$propuesta->Canal_Venta}}</strong>. <br> (PAUSA) <br> ¿Es correcto? <strong>(si dice que NO,
confirmar la información y ofrecer la anulación del seguro si fue una venta no reconocida o mal informada)</strong>
</p>

<div class="row">
	<div class="form-group">
    <select  class="form-control" id="conn" name="conn">
    <option value="null">seleccione</option>
      <option value="reconoce">si reconoce</option>
      <option value="noreconoce">no reconoce</option>
      <option value="renuncia">Renuncia</option>
      <option value="tercero">Tercero</option>
    </select>
  </div>
</div>


<div id="reconoce" style="display: none">
¿Tiene 2 minutos para responder unas breves preguntas que nos permitirán mejorar nuestro servicio? (si dice
que sí continuar, sino pasar a despedida) Perfecto. Le informo que, por motivos de calidad, esta llamada
estará siendo grabada.<br><br>
- En una escala de 1 a 7<br>


<div class="row">
	<div class="form-group">
    <label for="preg1">¿cuán dispuesto/a estaría a recomendar la contratación de este seguro a sus
familiares o amigos?</label>
    <select  class="form-control" id="preg1" name="preg1">
      <option value="null">seleccione respuesta</option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
      <option value="6">6</option>
      <option value="7">7</option>
    </select>
  </div>
</div>

<br><br>
- ¿A qué se debe esta evaluación?<br>
(REGISTRAR EN CAMPO "OBSERVACIÓN ENCUESTA") <br><br><br>


@if(trim($propuesta->ambiente) == "IAXIS")
¿Recibió en su correo la póliza? el documento que contiene las características
de su seguro?<br>
<div class="row">
  <div class="form-group">
    <label for="poliza_recepcion">el formato de la información</label>
    <select  class="form-control" id="poliza_recepcion" name="poliza_recepcion">
      <option value="">seleccione respuesta</option>
      <option value="si">SI</option>
      <option value="no">NO</option>
    </select>
  </div>
</div>
<br><br>

<div id="si-p1" style="display: none">
En una escala de 1 a 7, ¿cómo evaluaría los siguientes aspectos respecto a la
información recibida?
<div class="row">
  <div class="form-group">
    <label for="preg2">el formato de la información</label>
    <select  class="form-control" id="preg2" name="preg2">
      <option value="null">seleccione respuesta</option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
      <option value="6">6</option>
      <option value="7">7</option>
    </select>
  </div>
</div>
<div class="row">
  <div class="form-group">
    <label for="preg3">el lenguaje utilizado</label>
    <select  class="form-control" id="preg3" name="preg3">
      <option value="null">seleccione respuesta</option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
      <option value="6">6</option>
      <option value="7">7</option>
    </select>
  </div>
</div>
<div class="row">
  <div class="form-group">
    <label for="preg1">La claridad en la información</label>
    <select  class="form-control" id="preg4" name="preg4">
      <option value="null">seleccione respuesta</option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
      <option value="6">6</option>
      <option value="7">7</option>
    </select>
  </div>
  </div>
<br><br>  
</div>
 


<div id="no-p1" style="display: none">
No hay problema, pediremos que le vuelvan a enviar la información al
<strong>{{$propuesta->email}}</strong> ¿está correcto? (revisar y corregir si es necesario) <br> La información
será enviada a más tardar dentro de las siguientes 24 horas hábiles.<br>  
<br><br>
</div>

@endif

Perfecto, muchas gracias <strong>{{$propuesta->Contratante}}</strong> por sus respuestas. <br>
<strong>DESPEDIDA:</strong> Le recuerdo que en santander.cl y en la app Santander Seguros puede revisar su póliza y las
principales coberturas de sus seguros. Muchas gracias por su tiempo. Que tenga un buen día
	
</div>



<div id="noreconoce" style="display: none">
	

Le informo que, por motivos de calidad, esta llamada estará siendo grabada. El seguro {{$propuesta->Producto}} se
visualiza contratado desde {{$propuesta->Fecha_Venta}} por el cliente {{$propuesta->Contratante}} de rut {{$propuesta->RUT}} (si es
sucursal) con el ejecutivo {{$propuesta->Vendedor}}. Debido a lo que usted me menciona, ¿confirma que no
reconoce la contratación o que desea anular este seguro?<br><br>
<strong>Sí, confirmo. RESPUESTA DEL CLIENTE CON SU PROPIA VOZ.</strong><br>
El seguro {{$propuesta->Producto}} con el n° de póliza <strong>{{$propuesta->Poliza}}</strong> quedará anulado en las próximas 72 horas, y en caso de<br>
que exista algún cobro asociado será devuelto en un plazo máximo de 15 días hábiles por medio de su cuenta
corriente del Banco Santander, o de lo contrario se emitirá un vale vista a su nombre. Si recibe la póliza
referente a este producto, rogamos que no considere la información.<br>

Lamentamos mucho los inconvenientes y le agradecemos el tiempo destinado a esta llamada.<br>
Que tenga un buen día.<br>
<br>
</div>

<div id="renuncia" style="display: none">
<strong>SI EL CLIENTE RECONOCE LA CONTRATACIÓN, PERO QUIERE RENUNCIAR AL SEGURO:</strong>
En ese caso, esta gestión la debe realizar llamando directamente a su ejecutivo o llamando al 600 320 3000. <br><br><br>

</div>


<div id="tercero" style="display: none">
<strong>SI CONTESTA UN TERCERO:</strong>
¿A qué hora podríamos contactarnos con él/ella en este número?<br>
Necesitamos hablar con el cliente titular para entregar la información.<br>
</div>




@section('javascriptInc')
    <script src={{ asset('/vendors/ckeditor_basic/ckeditor.js') }}></script>





    <script defer>
        $(document).ready(function(){
            $("select[name='conn']").change(function(){
                if(this.value == 'reconoce'){
                    $('#reconoce').css('display','block');
                    $('#renuncia').css('display','none');
                    $('#noreconoce').css('display','none');
                    $('#tercero').css('display','none');
                } else if(this.value == 'noreconoce'){
                    $('#reconoce').css('display','none');
                    $('#renuncia').css('display','none');
                    $('#noreconoce').css('display','block');
                    $('#tercero').css('display','none');
                } else if(this.value == 'renuncia'){
                    $('#reconoce').css('display','none');
                    $('#renuncia').css('display','block');
                    $('#noreconoce').css('display','none');
                    $('#tercero').css('display','none');
                }else if(this.value == 'tercero'){
                    $('#reconoce').css('display','none');
                    $('#renuncia').css('display','none');
                    $('#noreconoce').css('display','none');
                    $('#tercero').css('display','block');
                }else{
                    $('#reconoce').css('display','none');
                    $('#renuncia').css('display','none');
                    $('#noreconoce').css('display','none');
                    $('#tercero').css('display','none');
                }
            });
            //no es conciente 2

            $("select[name='poliza_recepcion']").change(function(){
                if(this.value == 'si'){
                    $('#si-p1').css('display','block');
                    $('#no-p1').css('display','none');
                }else if(this.value == 'no'){
                    $('#si-p1').css('display','none');
                    $('#no-p1').css('display','block');
                    
                }else{
                    $('#si-p1').css('display','none');
                    $('#no-p1').css('display','none');
                    
                }
            })

            $("select[name='conn-2']").change(function(){
                if(this.value == 'si'){
                    $('#si-2').css('display','block');
                    $('#pa').css('display','none');
                    $('#vncso').css('display','none');
                    $('#dup').css('display','none');
                }else if(this.value == 'pa'){
                    $('#si-2').css('display','none');
                    $('#pa').css('display','block');
                    $('#vncso').css('display','none');
                    $('#dup').css('display','none');
                }else if(this.value == 'vncso'){
                    $('#si-2').css('display','none');
                    $('#pa').css('display','none');
                    $('#vncso').css('display','block');
                    $('#dup').css('display','none');
                }else if(this.value == 'dup'){
                    $('#si-2').css('display','none');
                    $('#pa').css('display','none');
                    $('#vncso').css('display','none');
                    $('#dup').css('display','block');
                }
            });

            //NOesConciente

            $("select[name='NOesConciente']").change(function(){
                if(this.value == '1'){
                    $('#si-rec').css('display','block');
                    $('#no-rec').css('display','none');
                }else if(this.value == '2'){
                    $('#si-rec').css('display','none');
                    $('#no-rec').css('display','block');
                }else{
                    $('#si-rec').css('display','none');
                    $('#no-rec').css('display','none');
                }
            });


            $("select[name='si-no']").change(function(){
                if(this.value == 'si'){
                    $('#si').css('display','block');
                    $('#no').css('display','none');
                }else if(this.value == 'no'){
                    $('#si').css('display','none');
                    $('#no').css('display','block');
                }else{
                    $('#si').css('display','none');
                    $('#no').css('display','none');
                }
            });

            $('#BuscarFechaVenta').click(function () {
                var FechaVenta=$('#fechaDiscado').val();
                var url=$('#BuscarFechaVenta').attr('href');


                if(FechaVenta  != ''){
                    fechaVentaInsert=url+'/'+FechaVenta;
                    $('#BuscarFechaVenta').attr('href',fechaVentaInsert);
                    return true;
                }else{
                    return false;
                }


            })

            $('#antes').click(function () {
                var FechaVenta=$('#fechaDiscado').val();
                var url=$('#antes').attr('href');


                if(FechaVenta  != ''){
                    fechaVentaInsert=url+'/'+FechaVenta;
                    $('#antes').attr('href',fechaVentaInsert);
                }

                return true;
            })


            $('#despues').click(function () {
                var FechaVenta=$('#fechaDiscado').val();
                var url=$('#despues').attr('href');


                if(FechaVenta  != ''){
                    fechaVentaInsert=url+'/'+FechaVenta;
                    $('#despues').attr('href',fechaVentaInsert);
                }

                return true;
            })





    $('.{{$propuesta->Canal_Venta}}').html(function () {
            $tipo = '{{$propuesta->Canal_Venta}}';

            if($tipo == '$tipo'){
                return $('#mostar1').html();
            }else if($tipo =='VOX CORREDORA' || $tipo == 'SUCURSAL' || $tipo == 'OPERATIVOS' || $tipo == 'EJECUTIVOS'){

                return $('#mostar1').html();
            }else if($tipo =='VOX'){
                return $('#mostar2').html();
            }else if($tipo == 'INTERNET' || $tipo == 'TELEMARKETING'){

                return $('#mostar3').html();
            }
    });

<?php
if ($propuesta->Canal_Venta == 'INTERNET') {
	echo "$('#internet-052019').hide();";
}
?>





//12042019
    $("#guardar").click(function(){
                var bv = $("#gestion").val();
                if(bv  == 1){
                  swal({
                          title: "Esta seguro de grabar",
                          text: "Propuesta, como buena venta, no podra modificarla",
                          icon: "warning",
                          buttons: true,
                          dangerMode: true,
                        })
                        .then((willDelete) => {
                          if (willDelete) {
                           $( "#formulario" ).submit();
                          } else {
                           return false;
                          }
                        });
                }else  if(bv  == 2){
                  swal({
                          title: "Esta seguro de grabar",
                          text: "Propuesta, venta imperfecta, no podra modificarla",
                          icon: "warning",
                          buttons: true,
                          dangerMode: true,
                        })
                        .then((willDelete) => {
                          if (willDelete) {
                           $( "#formulario" ).submit();
                          } else {
                           return false;
                          }
                        });
                }else{
                    $( "#formulario" ).submit();
                }
            })
    })

    $("select[name='preg1']").change(function(){
        var valor = $(this).val()

        if(valor >= 1 && valor <= 4){
            $("#preg1-1").show()
            $("#preg1-2").hide()
            $("#preg1-3").hide()
        }else if(valor == 5){
            $("#preg1-1").hide()
            $("#preg1-2").show()
            $("#preg1-3").hide()
        }else if(valor == 6 || valor == 7){
            $("#preg1-1").hide()
            $("#preg1-2").hide()
            $("#preg1-3").show()
        }else{
            $("#preg1-1").hide()
            $("#preg1-2").hide()
            $("#preg1-3").hide()
        }
    })

    </script>
@endsection

