@extends('layouts.admin')

@section('posicion_url')
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12  text-white">
            <h1 class="page-header">
                <p class="text-white">Propuestas <small>Panel Busqueda Propuestas especial </small></p>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="{{route('home')}}" >Inicio </a> > Propuestas

                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
    <!-- panel Busqueda -->
    {{Form::open(['route' => 'buscarAdminPropuestas'])}}

    {{ csrf_field() }}
    <div class="panel panel-info">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-2 col-md-4 col-lg-4">
                    <div class="input-group">
                        <input id="propuesta" name="propuesta" required="required" type="number"   />
                    </div>
                </div>
                    <div class="col-xs-2 col-md-4 col-lg-4">
                    <div class="input-group-btn">
                        {{Form::submit('Buscar',['class'=>'btn btn-primary'])}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- panel Busqueda -->
    {{Form::close()}}
<br>

    <!-- /.row -->
@endsection

@section('javascriptInc')


@endsection