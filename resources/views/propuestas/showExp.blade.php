@extends('layouts.admin')

@section('posicion_url')
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Propuestas <small>Panel Gestión Propuestas</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="{{route('home')}}" >Inicio </a> > Propuestas

                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')

    <!-- panel Busqueda -->
    {{Form::open(['route' => 'buscarPropuesta'])}}
    {{Form::hidden('id', $propuesta->id) }}
    <div class="panel panel-info">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-2 col-md-4 col-lg-4">
                    <div class="input-group">
                        {{Form::text('txtBuscar',$propuesta->No_Propuesta,['class' => 'form-control'])}}

                         <div class="input-group-btn">
                        {{Form::submit('Propuesta',['class'=>'btn btn-primary'])}}
                        </div>
                        &ensp;@if($propuesta->id > 1)
                        <div  class="input-group-btn">
                            <a href="{{asset('propuestas/ant/'.$propuesta->id)}}" id="antes" class='btn btn-primary'><span class="glyphicon glyphicon-triangle-left" aria-hidden="true"></span></a>
                        </div>
                        @else
                            <div  class="input-group-btn">
                                <span class='btn btn-primary'><span class="glyphicon glyphicon-triangle-left" aria-hidden="true"></span></span>
                            </div>
                        @endif
                        &ensp;
                        <div class="input-group-btn">
                            <a href="{{asset('propuestas/sig/'.$propuesta->id)}}" id="despues" class='btn btn-primary'><span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span></a>
                        </div>

                    </div>
                </div>
                <div class="col-md-2 col-lg-2">
                    <span class="label label-warning">Recorrido Diario {{$PorcRecorrido}}% </span><br>
                </div>
                <div class="col-md-4 col-lg-4">
                    <div class="form-group">
                        <label>Fecha Ventas</label>
                        @if($FechaVenta != '')
                        <input class="form-control" type="date" value="{{Carbon\Carbon::parse($FechaVenta)->format('Y-m-d')}}"  id="fechaDiscado" >
                        @else
                            <input class="form-control" type="date"   id="fechaDiscado" >
                            @endif
                    </div>
                </div>
                    <div class="col-md-2 col-lg-2">
                        <div class="form-group">
                            <label> Busqueda </label><br>
                            <a href="{{asset('propuestas/fechaVenta/')}}" id="BuscarFechaVenta" class="btn btn-success">Buscar</a>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <!-- panel Busqueda -->
    {{Form::close()}}
<br>
    <form id="formulario" method="POST" action="{{ asset('propuestas/'.$propuesta->id) }}" enctype="multipart/form-data">
        <input name="_method" type="hidden" value="PUT">
        {{ csrf_field() }}
    <div class="col-lg-12">
                    <div class="form-group">
                        <br>
                        <a id="guardar"  class="btn btn-primary">
                            Guardar
                        </a>
                    </div>

                </div>

    <div class="panel">
        <div class="panel-body">
            <div class="row">

                <div class="col-lg-4">

                    <div class="form-group">
                        <label>Nombre</label>
                        <input class="form-control" value="{{$propuesta->Contratante}}"  disabled>

                    </div>

                    <div class="form-group">
                        <label>Telefono</label>
                        <input class="form-control" value="{{"#".$propuesta->No_Propuesta."#".$propuesta->Telefono}}" disabled>
                    </div>

                    <div class="form-group">
                        <label>E-Mail</label>
                        <input class="form-control" type="email" name="email" value="{{$propuesta->email}}" >
                    </div>




                </div>


                <div class="col-lg-4">

                    <div class="form-group">
                        <label>Producto</label>
                        <input class="form-control" value="{{$propuesta->Producto}}"  disabled>

                    </div>



                    <div class="form-group">
                        <label>Monto Prima / Valor</label>
                        <input class="form-control" value="{{$propuesta->Prima}}" disabled>

                    </div>

                    <div class="form-group">
                        <label>Fecha Ultima Llamada</label>
                        <input class="form-control" type="date" value="{{Carbon\Carbon::parse($propuesta->Fecha_Llamada)->format('Y-m-d')}}" disabled >
                    </div>


                    @if($propuesta->idCampania == 2)
                        <div class="form-group">
                            <label>Marca</label>
                            <input class="form-control" type="text" value="{{$propuesta->Marca }}"  disabled >
                        </div>
                        <div class="form-group">
                            <label>Modelo</label>
                            <input class="form-control" type="text" value="{{$propuesta->Modelo}}" disabled >
                        </div>

                        <div class="form-group">
                            <label>Patente</label>
                            <input class="form-control" type="text" value="{{$propuesta->Patente}}"  disabled >
                        </div>


                    @endif

                </div>

              <div class="col-lg-4">

                    <div class="form-group">
                        <label class="control-label" name="Gestion">Gestion</label>

                        {{Form::select(($propuesta->PBV==1)?'GestionBlock':'Gestion',['1'=>'Buena Venta',
                        '2'=>'Venta imperfecta',
                        //'3'=>'Venta forzada',
                        //'4'=>'Equivocado',
                        //'5'=>'Buzon de voz',
                        //'6'=>'Fuera de servicio',
                        '7'=>'Rechaza bienvenida',
                        //'8'=>'No conectado',
                        '9'=>'No contesta',
                        '10'=>'Numero malo',
                        //'11'=>'Venta imperfecta',
                        '12'=>'Volver a llamar',
                        //'13'=>'Contacto ejecutivo',
                        '14'=>'Contacto tercero',
                         '15'=>'No Contactado'
                        ],$propuesta->PBV,['id'=>'gestion','class'=>'form-control' ,'placeholder'=>'Seleccione Gestion' ,($propuesta->PBV==1)?'disabled':''])}}
                        @if($propuesta->PBV==1)
                        <input type="hidden" name="Gestion" name="Gestion" value="{{$propuesta->PBV}}" >
                        @endif


                    </div>






                    <div class="form-group">
                        <label>Tipo Rechazo</label>
                        {{ Form::select(($propuesta->PBV==1)?'Motivo_RechazoBlock':'Motivo_Rechazo',[
                        ''=>'',
                        'Apertura de Cuenta'=>'Apertura de Cuenta',
                        'Avance'=>'Avance',
                        'Cotización'=>'Cotización',
                        'Duplicidad'=>'Duplicidad',
                        'Anulacion'=>'Anulacion',
                        'No reconoce Contratación'=>'No reconoce Contratación',
                        'Otro trámite'=>'Otro trámite',
                        'Préstamo / Crédito'=>'Préstamo / Crédito',
                        'Repactación'=>'Repactación',
                        'Venta no cerrada'=>'Venta no cerrada'
                        ],$propuesta->Motivo_Rechazo,['class'=>'form-control',($propuesta->PBV==1)?'disabled':''])}}

                         @if($propuesta->PBV==1)
                        <input type="hidden" name="Motivo_Rechazo" name="Motivo_Rechazo" value="{{$propuesta->Motivo_Rechazo}}" >
                        @endif

                    </div>
                    <div class="form-group">
                        <label>Fecha Venta</label>
                        <input class="form-control"  type="date"  value="{{Carbon\Carbon::parse($propuesta->Fecha_Venta)->format('Y-m-d')}}" disabled >
                    </div>


                </div>
            </div>

<hr>

     @if($propuesta->idCampania != 2)
    <!-- panel telefonos -->
    <div class="panel panel-primary">
    <div class="row">

        <div class="col-md-4 col-lg-4">
            <!-- contacto 1 -->
            <div class="form-group input-group ">
                <span class="input-group-addon">Tel. Contacto 1</span>
                <input type="text" class="form-control" name="Telefono_CRM_1" readonly="readonly" value="{{$propuesta->Area_1}}-{{$propuesta->Telefono_CRM_1}}">
            </div>
            <!-- contacto 1 -->
            <!-- contacto 2 -->
            <div class="form-group input-group">
                <span class="input-group-addon">Tel. Contacto 2</span>
                <input type="text" class="form-control " name="Telefono_CRM_2" readonly="readonly" value="{{$propuesta->Area_2}}-{{$propuesta->Telefono_CRM_2}}" >
            </div>
            <!-- contacto 2 -->
            <!-- contacto 3 -->
            <div class="form-group input-group">
                <span class="input-group-addon">Tel. Contacto 3</span>
                <input type="text" class="form-control" name="Telefono_CRM_3" readonly="readonly" value="{{$propuesta->Area_3}}-{{$propuesta->Telefono_CRM_3}}">
            </div>
            <!-- contacto 3 -->
            <!-- contacto 4 -->
            <div class="form-group input-group">
                <span class="input-group-addon">Tel. Contacto 4</span>
                <input type="text" class="form-control" name="Telefono_CRM_4" readonly="readonly" value="{{$propuesta->Area_4}}-{{$propuesta->Telefono_CRM_4}}">
            </div>
            <!-- contacto 4 -->
            <!-- contacto 5 -->
            <div class="form-group input-group">
                <span class="input-group-addon">Tel. Contacto 5</span>
                <input type="text" class="form-control" name="Telefono_CRM_5" readonly="readonly" value="{{$propuesta->Area_5}}-{{$propuesta->Telefono_CRM_5}}">
            </div>
            <!-- contacto 5 -->

        </div>



        <div class="col-md-4 col-lg-4">
            <!-- contacto 1 -->
            <div class="form-group input-group">
            <span class="input-group-addon">Status 1</span>
                {{ Form::select('NoConecta_Tel1',[
                        ''=>'',
                        'Anexo No Funciona'=>'Anexo No Funciona',
                        'Buzón de voz'=>'Buzón de voz',
                        'Congestionado'=>'Congestionado',
                        'Extension no disponible'=>'Extension no disponible',
                        'Fax'=>'Fax',
                        'Fuera de servicio'=>'Fuera de servicio',
                        'No contesta'=>'No contesta',
                        'Número incompleto'=>'Número incompleto',
                        'Número no existe'=>'Número no existe',
                        'Número equivocado'=>'Número equivocado',
                        'option'=>'option',
                        'Vacante'=>'Vacante'
                        ],
                        $propuesta->NoConecta_Tel1,['class'=>'form-control'])}}

            </div>
            <!-- contacto 1 -->

            <!-- contacto 2 -->
            <div class="form-group input-group">
                <span class="input-group-addon">Status 2</span>
                {{ Form::select('NoConecta_Tel2',[
                        ''=>'',
                        'Anexo No Funciona'=>'Anexo No Funciona',
                        'Buzón de voz'=>'Buzón de voz',
                        'Congestionado'=>'Congestionado',
                        'Extension no disponible'=>'Extension no disponible',
                        'Fax'=>'Fax',
                        'Fuera de servicio'=>'Fuera de servicio',
                        'No contesta'=>'No contesta',
                        'Número incompleto'=>'Número incompleto',
                        'Número no existe'=>'Número no existe',
                        'Número equivocado'=>'Número equivocado',
                        'option'=>'option',
                        'Vacante'=>'Vacante'
                        ],
                        $propuesta->NoConecta_Tel2,['class'=>'form-control'])}}
            </div>
            <!-- contacto 2 -->

            <!-- contacto 3 -->
            <div class="form-group input-group">
                <span class="input-group-addon">Status 3</span>
                {{ Form::select('NoConecta_Tel3',[
                        ''=>'',
                        'Anexo No Funciona'=>'Anexo No Funciona',
                        'Buzón de voz'=>'Buzón de voz',
                        'Congestionado'=>'Congestionado',
                        'Extension no disponible'=>'Extension no disponible',
                        'Fax'=>'Fax',
                        'Fuera de servicio'=>'Fuera de servicio',
                        'No contesta'=>'No contesta',
                        'Número incompleto'=>'Número incompleto',
                        'Número no existe'=>'Número no existe',
                        'Número equivocado'=>'Número equivocado',
                        'option'=>'option',
                        'Vacante'=>'Vacante'
                        ],
                        $propuesta->NoConecta_Tel3,['class'=>'form-control'])}}
            </div>
            <!-- contacto 3 -->

            <!-- contacto 4 -->
            <div class="form-group input-group">
                <span class="input-group-addon">Status 4</span>
                {{ Form::select('NoConecta_Tel4',[
                         ''=>'',
                         'Anexo No Funciona'=>'Anexo No Funciona',
                         'Buzón de voz'=>'Buzón de voz',
                         'Congestionado'=>'Congestionado',
                         'Extension no disponible'=>'Extension no disponible',
                         'Fax'=>'Fax',
                         'Fuera de servicio'=>'Fuera de servicio',
                         'No contesta'=>'No contesta',
                         'Número incompleto'=>'Número incompleto',
                         'Número no existe'=>'Número no existe',
                         'Número equivocado'=>'Número equivocado',
                         'option'=>'option',
                         'Vacante'=>'Vacante'
                         ],
                         $propuesta->NoConecta_Tel4,['class'=>'form-control'])}}
            </div>
            <!-- contacto 4 -->

            <!-- contacto 5 -->
            <div class="form-group input-group">
                <span class="input-group-addon">Status 5</span>
                {{ Form::select('NoConecta_Tel5',[
                         ''=>'',
                         'Anexo No Funciona'=>'Anexo No Funciona',
                         'Buzón de voz'=>'Buzón de voz',
                         'Congestionado'=>'Congestionado',
                         'Extension no disponible'=>'Extension no disponible',
                         'Fax'=>'Fax',
                         'Fuera de servicio'=>'Fuera de servicio',
                         'No contesta'=>'No contesta',
                         'Número incompleto'=>'Número incompleto',
                         'Número no existe'=>'Número no existe',
                         'Número equivocado'=>'Número equivocado',
                         'option'=>'option',
                         'Vacante'=>'Vacante'
                         ],
                         $propuesta->NoConecta_Tel5,['class'=>'form-control'])}}
            </div>
            <!-- contacto 5 -->

        </div>

    </div>
    </div>
        @endif
    <!-- panel telefonos -->

<hr>
            <div class="row">

                <div class="col-lg-6">

                    <div class="form-group">
                        <label>Agente Asignado</label>
                        <input class="form-control" value="{{$propuesta->Asesor_Corner}}" disabled>
                    </div>

                </div>



                <div class="col-lg-6">

                    <div class="form-group">
                        <label>Fecha Carga</label>
                        <input class="form-control" type="date" value="{{Carbon\Carbon::parse($propuesta->Fecha_Base)->format('Y-m-d')}}" disabled >
                    </div>


               </div>

           </div>

           <div class="row">
               <div class="col-lg-6">
                   <div class="form-group">
                       <label>Observación Encuesta</label>
                       <textarea class="form-control"  name="obsencuesta" id="obsencuesta" rows="5" cols="20">{{$propuesta->Observaciones2}}</textarea>
                    </div>
                </div>
                <div class="col-lg-6">
                   <div class="form-group">
                       <label>Observación </label>
                       <textarea class="form-control"  name="editor1" id="editor1" rows="5" cols="20">{{strip_tags($propuesta->Observaciones)}}</textarea>
                    </div>
                </div>

            </div>
        </div>
    </div>
<!-- segundo panel -->

<div class="row">
    <div class="col-lg-12">

        <div class="page-header">
            <h1>Script</h1>
        </div>
        <div class="well">

            @include('propuestas.scriptExpansion')
        </div>

    </div>

</div>

    </form>


    <!-- /.row -->
@endsection

