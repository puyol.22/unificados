@extends('layouts.admin')

@section('posicion_url')
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Propuestas <small style="color: #fff">Busquerda administrador</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="{{route('home')}}" >Inicio </a> > Propuestas busqueda administrador

                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
    <!-- panel Busqueda -->
    {{Form::open(['route' => 'buscarAdminPropuestas'])}}
    {{Form::hidden('id', $propuesta->id) }}
    <div class="panel panel-info">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-2 col-md-4 col-lg-4">
                        <div class="input-group">
                        {{Form::text('propuesta',$propuesta->No_Propuesta,['class' => 'form-control'])}}

                         <div class="input-group-btn">
                        {{Form::submit('Propuesta',['class'=>'btn btn-primary'])}}
                        </div>
                        &ensp;@if($propuesta->id > 1)
                        <div  class="input-group-btn">
                            <a href="{{asset('propuestas/ant/'.$propuesta->id)}}" id="antes" class='btn btn-primary'><span class="glyphicon glyphicon-triangle-left" aria-hidden="true"></span></a>
                        </div>
                        @endif

                    </div>
                </div>
                <div class="col-md-2 col-lg-2">
                    <span class="label label-warning">Recorrido Diario {{$PorcRecorrido}}% </span><br>
                </div>


                </div>
            </div>
        </div>
    </div>
    <!-- panel Busqueda -->
    {{Form::close()}}
<br>


    <form method="POST" action="{{ asset('propuestas/'.$propuesta->id.'/admin') }}" enctype="multipart/form-data">
        <input name="_method" type="hidden" value="PUT">
        {{ csrf_field() }}


    <div class="panel panel-primary">
        <div class="panel-body">

            <div class="row">

                <div class="col-lg-4">

                    <div class="form-group">
                        <label>Nombre</label>
                        <input class="form-control" value="{{$propuesta->Contratante}}"  disabled>

                    </div>

                    <div class="form-group">
                        <label>Telefono</label>
                        <input class="form-control" value="{{"#".$propuesta->No_Propuesta."#".$propuesta->Telefono}}" disabled>
                    </div>

                    <div class="form-group">
                        <label>E-Mail</label>
                        <input class="form-control" type="email" name="email" value="{{$propuesta->email}}" >
                    </div>

                    <div class="form-group">
                        <label>Canal de Venta</label>
                        <input class="form-control" value="{{$propuesta->Canal_Venta}}" disabled >
                    </div>

                    <div class="form-group">
                        <label>Fecha Ultima Llamada</label>
                        <input class="form-control" name="ultima_llamada" type="date" value="{{Carbon\Carbon::parse($propuesta->Fecha_Llamada)->format('Y-m-d')}}"  >
                    </div>


                </div>


                <div class="col-lg-4">

                    <div class="form-group">
                        <label>Producto</label>
                        <input class="form-control" value="{{$propuesta->Producto}}"  disabled>

                    </div>

                    <div class="form-group ">
                        <label class="control-label">DSCPLAN</label>
                        <input class="form-control" value="{{$propuesta->DscPlan}}" disabled>

                    </div>

                    <div class="form-group">
                        <label>Monto Prima / Valor</label>
                        <input class="form-control" value="{{$propuesta->Prima}}" disabled>

                    </div>
                    @if($propuesta->idCampania != 2)
                    <div class="form-group">
                        <label>Codigo Producto</label>
                        <input class="form-control" value="{{$propuesta->Codigo_Producto}}" disabled>

                    </div>
                    @endif
                    @if($propuesta->idCampania == 2)
                        <div class="form-group">
                            <label>Marca</label>
                            <input class="form-control" type="text" value="{{$propuesta->Marca }}"  disabled >
                        </div>
                        <div class="form-group">
                            <label>Modelo</label>
                            <input class="form-control" type="text" value="{{$propuesta->Modelo}}" disabled >
                        </div>

                        <div class="form-group">
                            <label>Patente</label>
                            <input class="form-control" type="text" value="{{$propuesta->Patente}}"  disabled >
                        </div>


                    @endif

                </div>

                <div class="col-lg-4">

                    <div class="form-group">
                        <label class="control-label" name="Gestion">Gestion</label>
                        {{Form::select('Gestion',[
                        null=>'Liberar',
                        '1'=>'Buena Venta',
                        '2'=>'Venta Imperfecta',
                        '3'=>'Venta Forzada',
                        '4'=>'Equivocado',
                        '5'=>'Buzon de Voz',
                        '6'=>'Fuera de Servicio',
                        '7'=>'Rechaza Bienvenida',
                        '8'=>'No Conectado',
                        '9'=>'No Contesta',
                        '10'=>'Numero no existe',
                        '11'=>'Venta Imperfecta',
                        '12'=>'Volver a Llamar',
                        '13'=>'Contacto ejectutivo',
                        '14'=>'Contacto tercero',
                        '15'=>'No Contactado',
                        ],$propuesta->PBV,['class'=>'form-control' ])}}

                    </div>

                    <div class="form-group">
                        <label>Estado Llamada</label>
                        {{Form::select('Conecta',[
                        ''=>'',
                        'Contactado'=>'Contactado',
                        'No Contactado'=>'No Contactado'
                        ],$propuesta->Conecta,['class'=>'form-control'])}}

                    </div>

                    <div class="form-group">
                        <label>Respuesta Contacto</label>
                        {{ Form::select('No_Conecta',
                        ['' => '',
                        'Anexo No Funciona'=>'Anexo No Funciona',
                        'Buzón de voz'=>'Buzón de voz',
                        'Congestionado'=>'Congestionado',
                        'Congestionado'=>'Congestionado',
                        'Extension no disponible'=>'Extension no disponible',
                        'Fax'=>'Fax',
                        'Fuera de servicio'=>'Fuera de servicio',
                        'No contesta'=>'No contesta',
                        'Número incompleto'=>'Número incompleto',
                        'Número no existe'=>'Número no existe',
                        'Número equivocado'=>'Número equivocado',
                        'Ocupado'=>'Ocupado',
                        'Vacante'=>'Vacante'
                        ],$propuesta->No_Conecta,['class'=>'form-control'])
                        }}

                    </div>

                    <div class="form-group">
                        <label>Estado Registro</label>
                        {{Form::select('Registro',['Abierto'=>'Abierto','Cerrado'=>'Cerrado'],$propuesta->Registro,['class'=>'form-control'])}}
                    </div>

                    <div class="form-group">
                        <label>Tipo Rechazo</label>
                        {{ Form::select('Motivo_Rechazo',[
                        ''=>'',
                        'Apertura de Cuenta'=>'Apertura de Cuenta',
                        'Avance'=>'Avance',
                        'Cotización'=>'Cotización',
                        'Duplicidad'=>'Duplicidad',
                        'Anulacion'=>'Anulacion',
                        'No reconoce Contratación'=>'No reconoce Contratación',
                        'Otro trámite'=>'Otro trámite',
                        'Préstamo / Crédito'=>'Préstamo / Crédito',
                        'Repactación'=>'Repactación',
                        'Venta no cerrada'=>'Venta no cerrada'
                        ],$propuesta->Motivo_Rechazo,['class'=>'form-control'])}}

                    </div>
                    <div class="form-group">
                        <label>Fecha Venta</label>
                        <input class="form-control" disabled  type="date"  value="{{Carbon\Carbon::parse($propuesta->Fecha_Venta)->format('Y-m-d')}}"  >
                    </div>

                </div>



        <!-- segundo panel -->
        @if($propuesta->idCampania != 2)
    <!-- panel telefonos -->
     <div class="panel panel-primary">
    <div class="row">

        <div class="col-md-4 col-lg-4">
            <!-- contacto 1 -->
            <div class="form-group input-group ">
                <span class="input-group-addon">Tel. Contacto 1</span>
                <input type="text" class="form-control" name="Telefono_CRM_1" value="{{$propuesta->Area_1}}-{{$propuesta->Telefono_CRM_1}} ">
            </div>
            <!-- contacto 1 -->
            <!-- contacto 2 -->
            <div class="form-group input-group">
                <span class="input-group-addon">Tel. Contacto 2</span>
                <input type="text" class="form-control " name="Telefono_CRM_2" value="{{$propuesta->Area_2}}-{{$propuesta->Telefono_CRM_2}}" >
            </div>
            <!-- contacto 2 -->
            <!-- contacto 3 -->
            <div class="form-group input-group">
                <span class="input-group-addon">Tel. Contacto 3</span>
                <input type="text" class="form-control" name="Telefono_CRM_3" value="{{$propuesta->Area_3}}-{{$propuesta->Telefono_CRM_3}}">
            </div>
            <!-- contacto 3 -->
            <!-- contacto 4 -->
            <div class="form-group input-group">
                <span class="input-group-addon">Tel. Contacto 4</span>
                <input type="text" class="form-control" name="Telefono_CRM_4" value="{{$propuesta->Area_4}}-{{$propuesta->Telefono_CRM_4}}">
            </div>
            <!-- contacto 4 -->
            <!-- contacto 5 -->
            <div class="form-group input-group">
                <span class="input-group-addon">Tel. Contacto 5</span>
                <input type="text" class="form-control" name="Telefono_CRM_5" value="{{$propuesta->Area_5}}-{{$propuesta->Telefono_CRM_5}}">
            </div>
            <!-- contacto 5 -->

        </div>



        <div class="col-md-4 col-lg-4">
            <!-- contacto 1 -->
            <div class="form-group input-group">
            <span class="input-group-addon">Status 1</span>
                {{ Form::select('NoConecta_Tel1',[
                        ''=>'',
                        'Anexo No Funciona'=>'Anexo No Funciona',
                        'Buzón de voz'=>'Buzón de voz',
                        'Congestionado'=>'Congestionado',
                        'Extension no disponible'=>'Extension no disponible',
                        'Fax'=>'Fax',
                        'Fuera de servicio'=>'Fuera de servicio',
                        'No contesta'=>'No contesta',
                        'Número incompleto'=>'Número incompleto',
                        'Número no existe'=>'Número no existe',
                        'Número equivocado'=>'Número equivocado',
                        'option'=>'option',
                        'Vacante'=>'Vacante'
                        ],
                        $propuesta->NoConecta_Tel1,['class'=>'form-control'])}}

            </div>
            <!-- contacto 1 -->

            <!-- contacto 2 -->
            <div class="form-group input-group">
                <span class="input-group-addon">Status 2</span>
                {{ Form::select('NoConecta_Tel2',[
                        ''=>'',
                        'Anexo No Funciona'=>'Anexo No Funciona',
                        'Buzón de voz'=>'Buzón de voz',
                        'Congestionado'=>'Congestionado',
                        'Extension no disponible'=>'Extension no disponible',
                        'Fax'=>'Fax',
                        'Fuera de servicio'=>'Fuera de servicio',
                        'No contesta'=>'No contesta',
                        'Número incompleto'=>'Número incompleto',
                        'Número no existe'=>'Número no existe',
                        'Número equivocado'=>'Número equivocado',
                        'option'=>'option',
                        'Vacante'=>'Vacante'
                        ],
                        $propuesta->NoConecta_Tel2,['class'=>'form-control'])}}
            </div>
            <!-- contacto 2 -->

            <!-- contacto 3 -->
            <div class="form-group input-group">
                <span class="input-group-addon">Status 3</span>
                {{ Form::select('NoConecta_Tel3',[
                        ''=>'',
                        'Anexo No Funciona'=>'Anexo No Funciona',
                        'Buzón de voz'=>'Buzón de voz',
                        'Congestionado'=>'Congestionado',
                        'Extension no disponible'=>'Extension no disponible',
                        'Fax'=>'Fax',
                        'Fuera de servicio'=>'Fuera de servicio',
                        'No contesta'=>'No contesta',
                        'Número incompleto'=>'Número incompleto',
                        'Número no existe'=>'Número no existe',
                        'Número equivocado'=>'Número equivocado',
                        'option'=>'option',
                        'Vacante'=>'Vacante'
                        ],
                        $propuesta->NoConecta_Tel3,['class'=>'form-control'])}}
            </div>
            <!-- contacto 3 -->

            <!-- contacto 4 -->
            <div class="form-group input-group">
                <span class="input-group-addon">Status 4</span>
                {{ Form::select('NoConecta_Tel4',[
                         ''=>'',
                         'Anexo No Funciona'=>'Anexo No Funciona',
                         'Buzón de voz'=>'Buzón de voz',
                         'Congestionado'=>'Congestionado',
                         'Extension no disponible'=>'Extension no disponible',
                         'Fax'=>'Fax',
                         'Fuera de servicio'=>'Fuera de servicio',
                         'No contesta'=>'No contesta',
                         'Número incompleto'=>'Número incompleto',
                         'Número no existe'=>'Número no existe',
                         'Número equivocado'=>'Número equivocado',
                         'option'=>'option',
                         'Vacante'=>'Vacante'
                         ],
                         $propuesta->NoConecta_Tel4,['class'=>'form-control'])}}
            </div>
            <!-- contacto 4 -->

            <!-- contacto 5 -->
            <div class="form-group input-group">
                <span class="input-group-addon">Status 5</span>
                {{ Form::select('NoConecta_Tel5',[
                         ''=>'',
                         'Anexo No Funciona'=>'Anexo No Funciona',
                         'Buzón de voz'=>'Buzón de voz',
                         'Congestionado'=>'Congestionado',
                         'Extension no disponible'=>'Extension no disponible',
                         'Fax'=>'Fax',
                         'Fuera de servicio'=>'Fuera de servicio',
                         'No contesta'=>'No contesta',
                         'Número incompleto'=>'Número incompleto',
                         'Número no existe'=>'Número no existe',
                         'Número equivocado'=>'Número equivocado',
                         'option'=>'option',
                         'Vacante'=>'Vacante'
                         ],
                         $propuesta->NoConecta_Tel5,['class'=>'form-control'])}}
            </div>
            <!-- contacto 5 -->

        </div>

    </div>
     </div>
        @endif
    <!-- panel telefonos -->




                <div class="col-lg-4">

                    <div class="form-group">
                        <label>Agente Asignado</label>
                        <input class="form-control" value="{{$propuesta->Asesor_Corner}}" disabled>
                    </div>

                </div>

                <div class="col-lg-4">

                    <div class="form-group">
                        <label>Tipo Documento</label>
                        {{Form::select('actualizaInformativo',['Digital'=>'Digital','Fisico'=>'Fisico'],$propuesta->actualizaInformativo,['class'=>'form-control'])}}
                    </div>

                </div>


                <div class="col-lg-4">

                    <div class="form-group">
                        <label>Fecha Carga</label>
                        <input class="form-control" type="date" value="{{Carbon\Carbon::parse($propuesta->Fecha_Base)->format('Y-m-d')}}" disabled >
                    </div>







               </div>

           </div>

           <div class="row">
               <div class="col-lg-6">
                <div class="form-group">
                       <label>Observación Encuesta</label>
                       <textarea class="form-control"  name="obsencuesta" id="obsencuesta" rows="5" cols="20">{{$propuesta->Observaciones2}}</textarea>
                    </div>
                </div>

                <div class="col-lg-6">
                <div class="form-group">
                       <label>Observación </label>
                       <textarea class="form-control"  name="editor1" id="editor1" rows="5" cols="20">{{strip_tags($propuesta->Observaciones)}}</textarea>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <br>
                        <button type="submit" class="btn btn-primary">
                            Guardar
                        </button>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">

            <div class="page-header">
                <h1>Script</h1>
            </div>
            <div class="well">
                @if($propuesta->idCampania == 1)
                @include('propuestas.scriptExpansion')
                @else
                @include('propuestas.scriptAuto')
                @endif
            </div>

        </div>

    </div>

</form>

    <div id="mostar1" style="display: none">
        <h2>Contratación sucursal o Fuerza de venta</h2>
    <p>Le informo que esta llamada no es para realizar ninguna venta sino para darle la bienvenida y agradecerle la reciente contratación del SS AUTOMOTRIZ DAÑOS PROPIOS que Ud. ha realizado en su sucursal a través del ejecutivo del Banco Santander, el día {{$propuesta->Fecha_Venta}} (Nombrar sucursal si cliente lo menciona)</p>

    </div>
    <div id="mostar2" style="display: none">
        <h2>Contratación vía telefónica o Tlmk:</h2>
        <P> Le informo que esta llamada no es para realizar ninguna venta sino para darle la bienvenida y agradecerle la reciente contratación del SS AUTOMOTRIZ DAÑOS PROPIOS que Ud. ha realizado a través de un llamado telefónico con el ejecutivo del Banco Santander, el día {{$propuesta->Fecha_Venta}}.</P>


    </div>
    <div id="mostar3" style="display: none">
        <h2>Contratación internet:</h2>
        <p>Le informo que esta llamada no es para realizar ninguna venta sino para darle la bienvenida y agradecerle la reciente contratación del SS AUTOMOTRIZ DAÑOS PROPIOS que Ud. ha realizado a través de la página web del Banco Santander, el día {{$propuesta->Fecha_Venta}} .</p>
    </div>
    <!-- /.row -->
@endsection

@section('javascriptInc')
    <script src="{{ asset('/vendors/ckeditor_basic/ckeditor.js') }}"></script>


    <script>
        $('.{{$propuesta->Canal_Venta}}').html(function () {
            $tipo = '{{$propuesta->Canal_Venta}}';
            if($tipo == ''){
                alert('!Canal de Venta vacio! coloquese en contacto con el Administrador.')
            }else if($tipo =='VOX CORREDORA' || $tipo == 'SUCURSAL' || $tipo == 'OPERATIVOS' || $tipo == 'EJECUTIVOS'){
                return $('#mostar1').html();
            }else if($tipo =='VOX'){
                return $('#mostar3').html();
            }else if($tipo == 'INTERNET' || $tipo == 'TELEMARKETING'){
                return $('#mostar2').html();
            }else{
                alert('Canal de Venta no encontrado! coloquese en contacto con el Administrador.')
            }
        });
    </script>

    <script>
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        //CKEDITOR.replace( 'editor1' );
    </script>

    <script>
        $(document).ready(function(){
            $("select[name='conn']").change(function(){
                if(this.value == 'si'){
                    $('#si-conn').css('display','block');
                    $('#no-conn').css('display','none');
                }else if(this.value == 'no'){
                    $('#si-conn').css('display','none');
                    $('#no-conn').css('display','block');
                }else{
                    $('#si-conn').css('display','none');
                    $('#no-conn').css('display','none');
                }
            });

            //NOesConciente

            $("select[name='NOesConciente']").change(function(){
                if(this.value == '1'){
                    $('#si-rec').css('display','block');
                    $('#no-rec').css('display','none');
                }else if(this.value == '2'){
                    $('#si-rec').css('display','none');
                    $('#no-rec').css('display','block');
                }else{
                    $('#si-rec').css('display','none');
                    $('#no-rec').css('display','none');
                }
            });


            $("select[name='si-no']").change(function(){
                if(this.value == 'si'){
                    $('#si').css('display','block');
                    $('#no').css('display','none');
                }else if(this.value == 'no'){
                    $('#si').css('display','none');
                    $('#no').css('display','block');
                }else{
                    $('#si').css('display','none');
                    $('#no').css('display','none');
                }
            });

            $('#BuscarFechaVenta').click(function () {
                var FechaVenta=$('#fechaDiscado').val();
                var url=$('#BuscarFechaVenta').attr('href');


                if(FechaVenta  != ''){
                    fechaVentaInsert=url+'/'+FechaVenta;
                    $('#BuscarFechaVenta').attr('href',fechaVentaInsert);
                    return true;
                }else{
                    return false;
                }


            })

            $('#antes').click(function () {
                var FechaVenta=$('#fechaDiscado').val();
                var url=$('#antes').attr('href');


                if(FechaVenta  != ''){
                    fechaVentaInsert=url+'/'+FechaVenta;
                    $('#antes').attr('href',fechaVentaInsert);
                }

                return true;
            })


            $('#despues').click(function () {
                var FechaVenta=$('#fechaDiscado').val();
                var url=$('#despues').attr('href');


                if(FechaVenta  != ''){
                    fechaVentaInsert=url+'/'+FechaVenta;
                    $('#despues').attr('href',fechaVentaInsert);
                }

                return true;
            })
        })



    </script>
@endsection