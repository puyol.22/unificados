<h4>SCRIPT PLAN DE BUENA VENTA AUTO</h4>

<p>Buenos días/tardes, ¿hablo con <strong>{{$propuesta->Contratante}}?</strong>?</p>
<p>Hola <strong>{{$propuesta->Contratante}}?</strong> mi nombre es <strong>{{$propuesta->User->name}}</strong> y la estoy llamando de Santander Seguros ¿cómo
está? (pausa) Que bueno. El motivo de mi llamada es para darle la bienvenida por la contratación del seguro
<strong>{{$propuesta->Producto}}?</strong> que realizó el día <strong>{{$propuesta->Fecha_Venta}}?</strong> a través de <strong>{{$propuesta->Canal_Venta}}?</strong>. (PAUSA)<br> 
¿Es correcto? (si dice que NO,
confirmar la información y ofrecer la anulación del seguro si fue una venta no reconocida o mal informada)<br>
SI EL CLIENTE RECONOCE LA CONTRATACIÓN:</p>



<div class="row">
	<div class="form-group">
    <select  class="form-control" id="conn" name="conn">
    <option value="null">seleccione</option>
      <option value="reconoce">Si reconoce</option>
      <option value="noreconoce">No reconoce</option>
      <option value="renuncia">Renuncia</option>
      <option value="tercero">Tercero</option>
       
    </select>
  </div>
</div>

<div>
	
</div>

<div id="reconoce" style="display: none"> 
<p>¿Tiene 1 minuto para responder unas breves preguntas que nos permitirán mejorar nuestro servicio? (si dice
que sí continuar, sino pasar a despedida) <br>

<strong>Perfecto</strong>. <br>
Le informo que, por motivos de calidad, esta llamada estará siendo grabada. <br>
- En una escala de 1 a 7</p>

<div class="row">
	<div class="form-group">
    <label for="preg1">¿cuán dispuesto/a estaría a recomendar la contratación de este seguro a sus
familiares o amigos?</label>
    <select  class="form-control" id="preg1" name="preg1">
      <option value="null">seleccione respuesta</option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
      <option value="6">6</option>
      <option value="7">7</option>
    </select>
  </div>
</div>

<h4>- ¿A qué se debe esta evaluación? (REGISTRAR EN RECUADRO "OBSERVACIÓN ENCUESTA")</h4>

<p>Perfecto, muchas gracias <strong>{{$propuesta->Contratante}}?</strong> por sus respuestas.</p>

<div> 
<p>
<h4>DESPEDIDA: </h4> Si tiene cualquier duda con respecto a su seguro, nos puede llamar al 600 320 3000 las 24 horas del día durante todo el año. Que tenga un buen día.
</p>
</div>

</div>



<div id="noreconoce" style="display: none"> 

<p >
Le informo que, por motivos de calidad, esta llamada estará siendo grabada. El seguro {{$propuesta->Producto}} se
visualiza contratado desde <strong>{{$propuesta->Fecha_Venta}}</strong> por el cliente <strong>{{$propuesta->Contratante}}</strong> de rut <strong>{{$propuesta->RUT}}</strong> (si es
sucursal) con el ejecutivo {{$propuesta->Vendedor}}. Debido a lo que usted me menciona, <br>
¿confirma que no reconoce la contratación o que desea anular este seguro? <br>

<!-- select -->

<strong>Sí, confirmo. RESPUESTA DEL CLIENTE CON SU PROPIA VOZ. </strong><br>
El seguro <strong>{{$propuesta->Producto}}</strong> con el n° de póliza <strong>{{$propuesta->No_Propuesta}}</strong> quedará anulado en sistema dentro de 7 días hábiles, y
en caso de que exista algún cobro asociado será devuelto en un plazo máximo de 15 días hábiles por medio
de su cuenta corriente del Banco Santander, o de lo contrario se emitirá un vale vista a su nombre. Si recibe
la póliza referente a este producto, rogamos que no considere la información.<br> 
Lamentamos mucho los inconvenientes y le agradecemos el tiempo destinado a esta llamada.<br> 
Que tenga un buen día. <br> <br>

</div>

<div id="renuncia" style="display: none" >

En ese caso, esta gestión la debe realizar llamando directamente a su ejecutivo o llamando al 600 320 3000.

</div>


<div id="tercero" style="display: none">
 
¿A qué hora podríamos contactarnos con él/ella en este número?<br> 
Necesitamos hablar con el cliente titular para entregar la información.<br> 
</div>

</p>

@section('javascriptInc')
    <script src={{ asset('/vendors/ckeditor_basic/ckeditor.js') }}></script>





    <script>
        $(document).ready(function(){
            $("select[name='conn']").change(function(){
                if(this.value == 'reconoce'){
                    $('#reconoce').css('display','block');
                    $('#renuncia').css('display','none');
                    $('#noreconoce').css('display','none');
                    $('#tercero').css('display','none');
                } else if(this.value == 'noreconoce'){
                    $('#reconoce').css('display','none');
                    $('#renuncia').css('display','none');
                    $('#noreconoce').css('display','block');
                    $('#tercero').css('display','none');
                } else if(this.value == 'renuncia'){
                    $('#reconoce').css('display','none');
                    $('#renuncia').css('display','block');
                    $('#noreconoce').css('display','none');
                    $('#tercero').css('display','none');
                }else if(this.value == 'tercero'){
                    $('#reconoce').css('display','none');
                    $('#renuncia').css('display','none');
                    $('#noreconoce').css('display','none');
                    $('#tercero').css('display','block');
                }else{
                    $('#reconoce').css('display','none');
                    $('#renuncia').css('display','none');
                    $('#noreconoce').css('display','none');
                    $('#tercero').css('display','none');
                }
            });

            //no es conciente 2

            $("select[name='conn-2']").change(function(){
                if(this.value == 'si'){
                    $('#si-2').css('display','block');
                    $('#pa').css('display','none');
                    $('#vncso').css('display','none');
                    $('#dup').css('display','none');
                }else if(this.value == 'pa'){
                    $('#si-2').css('display','none');
                    $('#pa').css('display','block');
                    $('#vncso').css('display','none');
                    $('#dup').css('display','none');
                }else if(this.value == 'vncso'){
                    $('#si-2').css('display','none');
                    $('#pa').css('display','none');
                    $('#vncso').css('display','block');
                    $('#dup').css('display','none');
                }else if(this.value == 'dup'){
                    $('#si-2').css('display','none');
                    $('#pa').css('display','none');
                    $('#vncso').css('display','none');
                    $('#dup').css('display','block');
                }
            });

            //NOesConciente

            $("select[name='NOesConciente']").change(function(){
                if(this.value == '1'){
                    $('#si-rec').css('display','block');
                    $('#no-rec').css('display','none');
                }else if(this.value == '2'){
                    $('#si-rec').css('display','none');
                    $('#no-rec').css('display','block');
                }else{
                    $('#si-rec').css('display','none');
                    $('#no-rec').css('display','none');
                }
            });


            $("select[name='si-no']").change(function(){
                if(this.value == 'si'){
                    $('#si').css('display','block');
                    $('#no').css('display','none');
                }else if(this.value == 'no'){
                    $('#si').css('display','none');
                    $('#no').css('display','block');
                }else{
                    $('#si').css('display','none');
                    $('#no').css('display','none');
                }
            });

            $('#BuscarFechaVenta').click(function () {
                var FechaVenta=$('#fechaDiscado').val();
                var url=$('#BuscarFechaVenta').attr('href');


                if(FechaVenta  != ''){
                    fechaVentaInsert=url+'/'+FechaVenta;
                    $('#BuscarFechaVenta').attr('href',fechaVentaInsert);
                    return true;
                }else{
                    return false;
                }


            })

            $('#antes').click(function () {
                var FechaVenta=$('#fechaDiscado').val();
                var url=$('#antes').attr('href');


                if(FechaVenta  != ''){
                    fechaVentaInsert=url+'/'+FechaVenta;
                    $('#antes').attr('href',fechaVentaInsert);
                }

                return true;
            })


            $('#despues').click(function () {
                var FechaVenta=$('#fechaDiscado').val();
                var url=$('#despues').attr('href');


                if(FechaVenta  != ''){
                    fechaVentaInsert=url+'/'+FechaVenta;
                    $('#despues').attr('href',fechaVentaInsert);
                }

                return true;
            })





    $('.{{$propuesta->Canal_Venta}}').html(function () {
            $tipo = '{{$propuesta->Canal_Venta}}';

            if($tipo == '$tipo'){
                return $('#mostar1').html();
            }else if($tipo =='VOX CORREDORA' || $tipo == 'SUCURSAL' || $tipo == 'OPERATIVOS' || $tipo == 'EJECUTIVOS'){

                return $('#mostar1').html();
            }else if($tipo =='VOX'){
                return $('#mostar2').html();
            }else if($tipo == 'INTERNET' || $tipo == 'TELEMARKETING'){

                return $('#mostar3').html();
            }
    });

<?php
if ($propuesta->Canal_Venta == 'INTERNET') {
	echo "$('#internet-052019').hide();";
}
?>





//12042019
    $("#guardar").click(function(){
                var bv = $("#gestion").val();
                if(bv  == 1){
                  swal({
                          title: "Esta seguro de grabar",
                          text: "Propuesta, como buena venta, no podra modificarla",
                          icon: "warning",
                          buttons: true,
                          dangerMode: true,
                        })
                        .then((willDelete) => {
                          if (willDelete) {
                           $( "#formulario" ).submit();
                          } else {
                           return false;
                          }
                        });
                }else  if(bv  == 2){
                  swal({
                          title: "Esta seguro de grabar",
                          text: "Propuesta, venta imperfecta, no podra modificarla",
                          icon: "warning",
                          buttons: true,
                          dangerMode: true,
                        })
                        .then((willDelete) => {
                          if (willDelete) {
                           $( "#formulario" ).submit();
                          } else {
                           return false;
                          }
                        });
                }else{
                    $( "#formulario" ).submit();
                }
            })
    })

    
    </script>
@endsection
