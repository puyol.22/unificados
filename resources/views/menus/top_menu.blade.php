<ul class="nav navbar-right top-nav">


    <li class="dropdown">

        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{\Illuminate\Support\Facades\Auth::user()->name}}<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <!--
            <li>
                <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
            </li>
            <li class="divider"></li>
        -->
            <li>
                <a href="{{route('logout')}}"><i class="fa fa-fw fa-power-off"></i> Salir</a>
            </li>
        </ul>
    </li>
</ul>
