<?php
$id = (\Illuminate\Support\Facades\Auth::user()->idtipousuario);
$accesoID = \App\Tipousuario::find($id);
?>
<div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">

                    <li @if (session(['active_menu' => 'home']))
                    class="active"
                    @endif  >
                        <a href="{{asset('home')}}"><i class="fa fa-fw fa-dashboard"></i> Inicio</a>
                    </li>
                    <?php
$accesoCRP = (($accesoID->id == 1) or ($accesoID->id == 2) or ($accesoID->id == 3)) ? true : false;
?>
                     <li>
                        <a href="{{asset('siniestros')}}"><i class="fa fa-fw fa-table"></i> Siniestros</a>
                    </li>
                    @if($accesoCRP)
                    <li  @if (session()->get('active_menu') == 'propuestas'))
                            class="active"
                            @endif  >

                        <a href="{{asset('propuestas')}}"><i class="fa fa-fw fa-table"></i> Propuestas</a>
                    </li>
                    @endif
                    <?php
$accesoCRPR = (($accesoID->id == 1) or ($accesoID->id == 2) or ($accesoID->id == 3)) ? true : false;
?>

                    @if($accesoCRPR)
                    <li @if (session()->get('active_menu') == 'reportes'))
                        class="active"
                            @endif >
                        <a href="{{asset('reportes')}}"><i class="fa fa-fw fa-bar-chart-o "></i> Reportes</a>
                    </li>
                    @endif

                    <?php
$accesoMAP = (($accesoID->id == 1) or ($accesoID->id == 2)) ? true : false;
?>

                    @if($accesoMAP)
                    <li @if (session()->get('active_menu') == 'buscarPropuestas'))
                        class="active"
                            @endif >
                        <a href="{{asset('propuestas/buscarAdmin  ')}}"><i class="fa fa-fw fa-search "></i> Buscar Propuestas</a>
                    </li>
                    @endif

                    <?php
$accesoCR = (($accesoID->id == 1) or ($accesoID->id == 2)) ? true : false;
?>
                    @if($accesoCR)
                    <li @if (session(['active_menu' => 'carga']))
                            class="active"
                            @endif  >
                        <a href="{{asset('carga')}}"><i class="fa fa-fw fa-edit"></i> Carga Registros</a>
                    </li>
                    @endif


                    <li @if (session(['active_menu' => 'grabaciones']))
                         class="active"
                            @endif >
                        <a href="{{asset('grabaciones')}}"><i class="glyphicon glyphicon-play-circle"></i> Grabaciones</a>
                    </li>
                    <li  @if (session(['active_menu' => 'mantenedor']))
                         class="active"
                            @endif >
                        <?php

$accesoU = (($accesoID->id == 1)) ? true : false;
?>
                        @if($accesoU)
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Mantenedor<i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="{{asset('mantenedor/usuario')}}">Usuarios</a>
                            </li>
                            <li>
                                <a href="{{asset('mantenedor/campania')}}">Campañas</a>
                            </li>
                            <li>
                                <a href="{{asset('mantenedor/tipousuario')}}">Tipos de Usuario</a>
                            </li>
                            <li>
                                <a href="{{asset('mantenedor/codigo')}}">Codigos Productos</a>
                            </li>

                        </ul>
                            @endif

                    </li>

                </ul>
</div>