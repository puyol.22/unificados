@extends('layouts.admin')

@section('posicion_url')
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Reportes <small> Campaña :: Todas </small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> Reportes
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')

    <!-- /.row -->
    {{Form::open(['url' => $url, 'method' => 'post','id'=>'formularioBusqueda'])}}
    <div class="panel panel-info">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-3">{{Form::select('mes',[
                '1'=>'Enero',
                '2'=>'Febrero',
                '3'=>'Marzo',
                '4'=>'Abril',
                '5'=>'Mayo',
                '6'=>'Junio',
                '7'=>'Julio',
                '8'=>'Agosto',
                '9'=>'Septiembre',
                '10'=>'Octubre',
                '11'=>'Noviembre',
                '12'=>'Diciembre'
                ],$mes,['class'=>'form-control','placeholder' => 'Selecciones Mes','required'])}}
                </div>

                <div class="col-lg-3">
                    <?php
                    $nuevafecha = strtotime ( '+5 year' , strtotime ( date('Y') ) ) ;
                    $nuevafecha = date('Y',$nuevafecha);
                    ?>
                    <select class="form-control" name="anio">
                        @for($i=2010; $i <= $nuevafecha ; $i++)
                            <option value="{{$i}}"
                                    @if($i == $ano)
                                    selected
                                    @endif
                            >{{$i}}</option>
                        @endfor
                    </select>

                </div>
                <div class="col-lg-3">
                    {{ Form::select('tipoCampania',[$campania],$tipoCampania,['class'=>'form-control'])  }}
                </div>
                <div class="col-lg-3">
                    {{Form::submit('REPORTE',['class'=>'btn btn-primary'])}}
                    {{Form::button('Excel',['class'=>'btn btn-success','id'=>'excel'])}}
                </div>
            </div>
        </div>
    </div>
    {{Form::close()}}

   <div class="row">
        <div class="panel panel-info">
        <div class="panel-body">
            <div class="row">
                <div class="col">
                    <div class="col-lg-3">
                    {{ Form::select('campaniaIAXIS',[$campania],'',['class'=>'form-control','id'=>'campaniaIAXIS'])  }}
                    </div>
                    <div class="col">
                        <div class="col-lg-3">
                    {{ Form::date('dateIAXIS',date('Y-m-d'),['class'=>'form-control','id'=>'dateIAXIS'])  }}
                    </div>
                    </div>
                    <div class="col">
                        <input type="butom" class="btn btn-primary" name="reporteIaxis" id="reporteIaxis" value="Reporte Iaxis">
                    </div>
                </div>        
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-comments fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{\App\Campania::all()->count()}}</div>
                            <div>Campañas Cargadas</div>
                        </div>
                    </div>
                </div>
                <a href="{{asset('mantenedor/campania')}}">
                    <div class="panel-footer">
                        <span class="pull-left">Campañas</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-tasks fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{\App\Propuestas::where('Mes',str_replace('0','',date('m')))->where('Ano',date('Y'))->count()}}</div>
                            <div>Registros Cargados</div>
                        </div>
                    </div>
                </div>
                <a href="{{asset('carga')}}">
                    <div class="panel-footer">
                        <span class="pull-left">Cargar Registros</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-check-square fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{\App\Propuestas::where('Mes',str_replace('0','',date('m')))->where('Ano',date('Y'))->where('Registro','Abierto')->count()}}</div>
                            <div>Registros Activos</div>
                        </div>
                    </div>
                </div>
                <a href="{{asset('carga')}}">
                    <div class="panel-footer">
                        <span class="pull-left">Carga Registros</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa  fa-list-alt fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{\App\Codigo::all()->count()}}</div>
                            <div>Codigos Cargados</div>
                        </div>
                    </div>
                </div>
                <a href="{{asset('mantenedor/codigo')}}">
                    <div class="panel-footer">
                        <span class="pull-left">Agregar Codigos</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <!-- /.row -->
 @if($tipoCampania == 99)
    <div class="content">
        <div class="row">
            @foreach($codigos as $cod)
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-money fa-fw"></i> {{ $cod->producto }}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>Order #</th>
                                    <th>Order Date</th>
                                    <th>Order Time</th>
                                    <th>Amount (USD)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>3326</td>
                                    <td>10/21/2013</td>
                                    <td>3:29 PM</td>
                                    <td>$321.33</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-right">
                            <a href="#">View All Transactions <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
                @endforeach
        </div>

        @endif


        <div class="row"  style="background-color: white;">
            <!-- TABLA REPORTE -->

            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Reporte Cumplimiento</h4>


                    <table class="table table-bordered table-striped table-hover table-condensed">
                        <thead>
                        <tr>

                            <th>Dias</th>
                            @foreach($dataReporte as $data)
                            <th>{{ $data->dias }}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>


                        <tr>
                            <td>Total Base</td>
                            @foreach($dataReporte as $data)
                                <td>{{ $data->total }}</td>
                            @endforeach
                        </tr>

                        <tr>
                            <td>BV</td>
                            @foreach($dataReporte as $data)
                                <td>{{ $data->total_dia }}</td>
                            @endforeach
                        </tr>
						
						 <tr>
                            <td>Vent Imp</td>
                            @foreach($dataReporte as $data)
                                <td>{{ $data->ventaimperfecta }}</td>
                            @endforeach
                        </tr>

                        <tr>
                            <td>% Cump</td>
                            @foreach($dataReporte as $data)
                                <td>{{ number_format($data->promedio,1,'.','') }}</td>
                            @endforeach
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <br>
<!-- TABLA REPORTE -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i> Reportes Gestion de Registros</h3>
                            </div>
                            <div class="panel-body">
                                <div id="bar-example"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i> Reportes Gestion de Registros Totales</h3>
                            </div>
                            <div class="panel-body">
                                <div id="donut-total"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6" >
                        <h2 style="color: #FFFFFF">Resumen</h2>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover ">
                                <thead>
                                <tr class="active">
                                    <th>Item</th>
                                    <th>Valor</th>


                                </tr>
                                </thead>
                                <tbody>
                                <tr class="active">
                                    <td>Total</td>
                                    <td>{{$total}}</td>


                                </tr>
                                <tr class="active">
                                    <td>Buena Venta</td>
                                    <td>{{$buenaventa}}</td>


                                </tr>
                                <tr class="active">
                                    <td>Venta Imperfecta</td>
                                    <td>{{$ventaimperfecta}}</td>


                                </tr>
                                <tr class="active">
                                    <td>Contactados</td>
                                    <td>{{$contactados}}</td>
                                </tr>
                                <tr class="active">
                                    <td>Cumplimiento</td>

                                    <?php
                                    $totalBV=$buenaventa+$ventaimperfecta;
                                    if($totalBV > 0){
                                    $cump=number_format(((($buenaventa+$ventaimperfecta)/$total)*100), 2, ',', ' ');
                                    }else{
                                        $cump=0;
                                    }
                                    ?>
                                    <td>{{ $cump }} %</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>



                </div>
        </div>




    <!-- /.row -->
@endsection


@section('javascriptInc')
    <!-- Morris Charts JavaScript -->
    <script src="{{asset('js/plugins/morris/raphael.min.js')}}"></script>
    <script src="{{asset('js/plugins/morris/morris.min.js')}}"></script>
    <!-- jQuery -->
    <script src={{asset("js/jquery.js")}}></script>

    <!-- Bootstrap Core JavaScript -->
    <script src={{asset("js/bootstrap.min.js")}}></script>

    <script defer>
        // Morris.js Charts sample data for SB Admin template

        $(function() {

            Morris.Bar({
                element: 'bar-example',
                data: {!! $grafico !!},
                xkey: 'dias',
                                            
                ykeys: ['total', 'total_dia','promedio'],
                labels: ['Llamadas', 'Total','%']
            });

            Morris.Donut({
                element: 'donut-total',
                data: [
                    {label: "Buena Venta", value: {!! $buenaventa !!}},
                    {label: "Venta Imperfecta", value: {!! $ventaimperfecta !!} },
                    {label: "Otros", value: {!! $otros !!} }
                ]
            });

        });

        $( document ).ready(function() {
            $("#excel").click(function () {
                formularioAux =$('#formularioBusqueda').attr('action');
                formulario = formularioAux+'/excel';
                $('#formularioBusqueda').attr('action',formulario);
                $('#formularioBusqueda').submit();
                $('#formularioBusqueda').attr('action',formularioAux);

            })

             $("#reporteIaxis").click(function(){
                campania=$("#campaniaIAXIS").val();
                fecha=$("#dateIAXIS").val();
                location.href = "{{asset('/reportes/iaxis/')}}"+'/'+campania+'/'+fecha;
            })
        });


    </script>

@endsection