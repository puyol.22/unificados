@extends('layouts.admin')

@section('posicion_url')
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Reportes <small> Campaña :: Todas </small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> Reportes
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')

    <!-- /.row -->
    {{Form::open(['url' => 'reportes/generar', 'method' => 'post'])}}
    <div class="panel panel-info">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-3">{{Form::select('mes',[
                '1'=>'Enero',
                '2'=>'Febrero',
                '3'=>'Marzo',
                '4'=>'Abril',
                '5'=>'Mayo',
                '6'=>'Junio',
                '7'=>'Julio',
                '8'=>'Agosto',
                '9'=>'Septiembre',
                '10'=>'Octubre',
                '11'=>'Noviembre',
                '12'=>'Diciembre'
                ],'',['class'=>'form-control','placeholder' => 'Selecciones Mes','required'])}}
                </div>

                <div class="col-lg-3">
                    <?php
                    $nuevafecha = strtotime ( '+5 year' , strtotime ( date('Y') ) ) ;
                    $nuevafecha = date('Y',$nuevafecha);
                    ?>
                    <select class="form-control" name="anio">
                        @for($i=2010; $i <= $nuevafecha ; $i++)
                            <option value="{{$i}}"
                                    @if($i == date('Y'))
                                    selected
                                    @endif
                             >{{$i}}</option>
                       @endfor
                    </select>

                </div>
                <div class="col-lg-3">
                    {{ Form::select('tipoCampania',$campania,'',['class'=>'form-control','placeholder'=>'Seleccione Campaña'])  }}
                </div>
                <div class="col-lg-3">
                    {{Form::submit('REPORTE',['class'=>'btn btn-primary'])}}
                </div>
            </div>
        </div>
    </div>
    {{Form::close()}}
    <!-- /.row -->

    <div class="row">
        <div class="panel panel-info">
        <div class="panel-body">
            <div class="row">
                <div class="col">
                    <div class="col-lg-3">
                    {{ Form::select('campaniaIAXIS',[$campania],'',['class'=>'form-control','id'=>'campaniaIAXIS'])  }}
                    </div>
                    <div class="col">
                        <div class="col-lg-3">
                    {{ Form::date('dateIAXIS',date('Y-m-d'),['class'=>'form-control' ,'id'=>'dateIAXIS'])  }}
                    </div>
                    </div>
                    <div class="col">
                        <input type="butom" class="btn btn-primary" name="reporteIaxis" id="reporteIaxis" value="Reporte Iaxis">
                    </div>
                </div>        
            </div>
        </div>
    </div>
    

    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-comments fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{\App\Campania::all()->count()}}</div>
                            <div>Campañas Cargadas</div>
                        </div>
                    </div>
                </div>
                <a href="{{asset('mantenedor/campania')}}">
                    <div class="panel-footer">
                        <span class="pull-left">Campañas</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-tasks fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{\App\Propuestas::where('Mes',str_replace('0','',date('m')))->where('Ano',date('Y'))->count()}}</div>
                            <div>Registros Cargados</div>
                        </div>
                    </div>
                </div>
                <a href="{{asset('carga')}}">
                    <div class="panel-footer">
                        <span class="pull-left">Cargar Registros</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-check-square fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{\App\Propuestas::where('Mes',str_replace('0','',date('m')))->where('Ano',date('Y'))->where('Registro','Abierto')->count()}}</div>
                            <div>Registros Activos</div>
                        </div>
                    </div>
                </div>
                <a href="{{asset('carga')}}">
                    <div class="panel-footer">
                        <span class="pull-left">Carga Registros</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa  fa-list-alt fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{\App\Codigo::all()->count()}}</div>
                            <div>Codigos Cargados</div>
                        </div>
                    </div>
                </div>
                <a href="{{asset('mantenedor/codigo')}}">
                    <div class="panel-footer">
                        <span class="pull-left">Agregar Codigos</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <!-- /.row -->



    <!-- /.row -->
@endsection
@section('javascriptInc')

    <!-- jQuery -->
    <script src={{asset("js/jquery.js")}}></script>



    <script defer>
        // Morris.js Charts sample data for SB Admin template

        $( document ).ready(function() {
        

             $("#reporteIaxis").click(function(){

                campania=$("#campaniaIAXIS").val();
                fecha=$("#dateIAXIS").val();
                location.href = '{{asset('/reportes/iaxis')}}'+'/'+campania+'/'+fecha;
            })
        });


    </script>

@endsection

