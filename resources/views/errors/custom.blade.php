@extends('layouts.admin')

@section('posicion_url')
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Error <small> Sistema  </small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> Error
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')

    <!-- /.row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-warning alert-dismissable">
                <i class="fa fa-info-circle"></i>  <strong>{{$mensaje}}</strong> Comuniquese con el administrador de sistema
            </div>
        </div>
    </div>
    <!-- /.row -->


    <!-- /.row -->



    <!-- /.row -->
@endsection


@section('javascriptInc')
    <!-- Morris Charts JavaScript -->
    <script src="{{asset('js/plugins/morris/raphael.min.js')}}"></script>
    <script src="{{asset('js/plugins/morris/morris.min.js')}}"></script>
    <script src="{{asset('js/plugins/morris/morris-data.js')}}"></script>

@endsection