@extends('layouts.admin')

@section('posicion_url')
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Carga <small> Listar ::  Archivos Cargados</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="{{route('home')}}" >Inicio </a> > Carga de Registros
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')

    <!-- /.row -->
    <div class="row">
        <?php
        echo Form::open(array('url' => asset('/carga'),'files'=>'true'));
        ?>
        <div class="col-lg-4">
            <div class="form-group" name="campania">
                <label>Camapañas</label>
                <select class="form-control" name="campania">
                    @foreach($campania as $camp)
                        <option value="{{$camp->idCampania}}">{{$camp->nombre}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-lg-4 ">
            <div class="form-group">
            <?php

            echo '<label>Seleccione Archivo</label>';
            echo Form::file('Exel',array('class'=>'form-control','required'));
            ?>
                    <br>
                <?php
                echo Form::submit('CARGAR ARCHIVO',array('class'=>'btn btn btn-success'));

                ?>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="col-md-6"><label>Mes</label>
                <select class="form-control" name="mes">
                    <option value="1"
                            @if(1 == date('m'))
                            selected
                            @endif
                    >Enero</option>
                    <option value="2"
                            @if(2 == date('m'))
                            selected
                            @endif
                    >Febrero</option>
                    <option value="3"
                            @if(3 == date('m'))
                            selected
                            @endif
                    >Marzo</option>
                    <option value="4"
                            @if(4 == date('m'))
                            selected
                            @endif
                    >Abril</option>
                    <option value="5"
                            @if(5 == date('m'))
                            selected
                            @endif
                    >Mayo</option>
                    <option value="6"
                            @if(6 == date('m'))
                            selected
                            @endif
                    >Junio</option>
                    <option value="7"
                            @if(7 == date('m'))
                            selected
                            @endif
                    >Julio</option>
                    <option value="8"
                            @if(8 == date('m'))
                            selected
                            @endif
                    >Agosto</option>
                    <option value="9"
                            @if(9 == date('m'))
                            selected
                            @endif
                    >Septiembre</option>
                    <option value="10"
                            @if(10 == date('m'))
                            selected
                            @endif
                    >Octubre</option>
                    <option value="11"
                            @if(11 == date('m'))
                            selected
                            @endif
                    >Noviembre</option>
                    <option value="12"
                            @if(12 == date('m'))
                            selected
                            @endif
                    >Diciembre</option>
                </select>
            </div>
            <div class="col-md-6"><label>Año</label>
                <?php
               $nuevafecha = strtotime ( '+5 year' , strtotime ( date('Y') ) ) ;
               $nuevafecha = date('Y',$nuevafecha);
                ?>
                <select class="form-control" name="ano">
                @for($i=2010; $i <= $nuevafecha ; $i++)
                    <option value="{{$i}}"
                    @if($i == date('Y'))
                        selected
                        @endif
                    >{{$i}}</option>
                @endfor
                </select>
            </div>
        </div>

        <?php
        echo Form::close();
        ?>
    </div>
    <!-- /.row -->
    <br>
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6 col-centered">
            <h2>CARGAS</h2>
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>NOMBRE</th>
                        <th>CANTIDAD CARGADO</th>
                        <th>FECHA CARGA</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($carga as $cargas)
                        <tr>
                            <td>{{$cargas->id}}</td>
                            <td>{{$cargas->nombre}}</td>
                            <td>{{$cargas->cantidadRegistro}}</td>
                            <td>{{$cargas->Updated_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-lg-3"></div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
        {{$carga->links()}}
        </div>
    </div>

    <!-- /.row -->
@endsection


@section('javascriptInc')
    <!-- Morris Charts JavaScript -->


@endsection