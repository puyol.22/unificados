<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

//Route::get('/uploadfile','UploadFileController@index');
//Route::post('/uploadfile','UploadFileController@showUploadFile');

Auth::routes();

Route::get('/', function () {
	return view('welcome');
});

Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/reportes', 'ReporteController@index')->name('ReporteIndex');

Route::post('/reportes/generar', 'ReporteController@generar')->name('ReporteVer');
Route::post('/reportes/generar/excel', 'ReporteController@generarExcel')->name('ReporteExel');

Route::get('/reportes/iaxis/{campania}/{fecha}', 'ReporteController@generarExcelIAXIS')->name('ReporteIndexIAXIS');

Route::get('/grabaciones', 'GrabacionesController@show')->name('mostrar Grabaciones');
Route::post('/grabaciones', 'GrabacionesController@_buscar')->name('buscar grabaciones');
Route::get('/grabacion/{grabacion}', 'GrabacionesController@_descargar')->name('descarga');

Route::get('propuestas/buscar', function () {
	return redirect()->route('propuestas.index');
});

Route::put('propuestas/{id}/admin', 'PropuestasController@updateAdmin');

Route::get('propuestas/buscarAdmin', 'PropuestasController@inicioBuscar');
Route::post('propuestas/buscarAdmin', 'PropuestasController@buscarPropuestas')->name('buscarAdminPropuestas');
Route::resource('propuestas', 'PropuestasController');
Route::resource('carga', 'CargaController');

Route::get('propuestas/sig/{id}/{fechaVenta?}', 'PropuestasController@propuestaSig');
Route::get('propuestas/ant/{id}/{fechaVenta?}', 'PropuestasController@propuestaAnt');

Route::get('propuestas/fechaVenta/{fechaVenta}', 'PropuestasController@propuestaFechaVenta');

Route::post('propuestas/buscar', 'PropuestasController@propuestaBuscar')->name('buscarPropuesta');

Route::prefix('mantenedor')->group(function () {
	Route::resource('empleados', 'EmpleadosController');
	Route::resource('tipousuario', 'TipousuarioController');
	Route::resource('usuario', 'UserController');
	Route::resource('codigo', 'CodigoController');
	Route::resource('campania', 'CampaniaController');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('siniestros','SiniestrosController');
