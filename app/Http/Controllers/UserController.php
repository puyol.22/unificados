<?php

namespace App\Http\Controllers;

use App\Campania;
use App\Tipousuario;
use App\User;
use Illuminate\Http\Request;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$usuario= new User();

        //

        return view('mantenedores.usuario',['user' => User::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('mantenedores.usuario_nuevo',['campania'=>Campania::all(),'campania'=>Campania::all(),'tipousuario'=> Tipousuario::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('mantenedores.usuario',['user' => User::all(),'campania'=>Campania::all(),'tipousuario'=> Tipousuario::all()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return view('mantenedores.usuario_editar',['user' => User::find($id),'campania'=>Campania::all(),'tipousuario'=> Tipousuario::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //

        $user= User::find($id);
        $user->name=$request->name;
        $user->email=$request->email;
        $user->password=bcrypt($request->password);
        $user->idcampania=$request->campania;
        $user->idtipousuario=$request->tipousuario;
        $saved = $user->save();
        if($saved) {
            return view('mantenedores.usuario', ['user' => User::all(), '']);
        }else{
            return view('errors.custom', ['mensaje' => 'ERROR AL ACTUALIZAR REGISTRO USUARIO #'.$id]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
