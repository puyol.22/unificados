<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GrabacionesController extends Controller
{
    //
    private $lineCommand;

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }


    public function show(){

        return view('grabaciones.show');
    }

    public function _buscar(Request $request)
    {
        $arrayComand = array();
        $getComand = "";

        if ($request->tipoBusqueda == 1) {
        /*$commands = [
            "cd /var/spool/asterisk/monitor/$request->anio/$request->mes/$request->dia/",
            "find . -type f -name '*#$request->grabacion#*.wav' "
        ];
        */
            $commands = [
                "cd /var/spool/asterisk/monitor/$request->anio/$request->mes/",
                "find . -type f -name '*#$request->grabacion#*.wav' "
            ];
        }else{
            $commands = [
                "cd /var/spool/asterisk/monitor/$request->anio/$request->mes/",
            "find . -type f -name '*$request->grabacion-*.wav' "
            ];

        }
        /*
        $commands = [
            "cd /var/spool/asterisk/monitor/$request->anio/$request->mes/30/",
            "ls"
        ];
        */

        //dd($commands);

        \SSH::run($commands, function($line)
        {
          $this->setCommand($line);

        });

        $getComand=$this->getCommand();
        $pos=strpos($getComand, 'bash:');


        $listCommand=explode("\n",$getComand);
        //dd($listCommand);



        if($pos === false){
        foreach ($listCommand as $lc) {

            $auxList="";
            $remotePath="";
            if(trim($lc) != ''){
                $auxList = explode('/', $lc);
                $arrayComand[]=array('anio'=>$request->mes,'mes'=>$request->mes,'dia'=>$auxList[1],'grabacion'=>$auxList[2],'ruta'=>$lc,'busqueda'=>$request->grabacion,'tipobusqueda'=>$request->tipoBusqueda);
                $remotePath="/var/spool/asterisk/monitor/$request->anio/$request->mes/".$lc;

              $localPath= public_path()."/grabacion/".$auxList[2];
              \SSH::into('production')->get($remotePath, $localPath);

             $contents = \SSH::into('production')->getString($remotePath);

            }

            //$arrayComand[]=array('anio'=>2018,'mes'=>$auxList[1],'dia'=>$auxList[2],'grabacion'=>$auxList[3],'ruta'=>$lc,'busqueda'=>'41118882','tipobusqueda'=>1);
        }
        }else{
            $arrayComand[]=array('anio'=>2018,'mes'=>$request->mes,'dia'=>'no encontrado','grabacion'=>"no encontrado",'ruta'=>"#",'busqueda'=>$request->grabacion,'tipobusqueda'=>$request->tipoBusqueda);
        }

        return view('grabaciones.grabaciones',['grab' => $arrayComand]);

    }

    public function _descargar($grabacion){
        $aux=str_replace("$","#",$grabacion);
        $pathtoFile = public_path().'/grabacion/'.$aux;
        return response()->download($pathtoFile);

    }

    private function setCommand($comand){
        $this->lineCommand=$comand;
    }
    private function getCommand(){
        return $this->lineCommand;
    }

}
