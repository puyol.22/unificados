<?php

namespace App\Http\Controllers;

use App\Campania;
use App\Codigo;
use App\Propuestas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Excel;

class ReporteController extends Controller {
	public $mes;
	public $anio;
	public $idCmapania;

	public function __construct() {
		$this->middleware('auth');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
		session(['active_menu' => 'reportes']);
		$campania = Campania::pluck('nombre', 'idCampania')->toArray();
		//dd($campania);
		return view('reportes.show', ['campania' => $campania]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	public function generar(Request $request) {
		//
		$arrayTotales = array();
		//totales
		$total = Propuestas::where('idCampania', $request->tipoCampania)->whereYear('Fecha_Venta', $request->anio)->whereMonth('Fecha_Venta', $request->mes)->count();
		$buenaVenta = Propuestas::where('idCampania', $request->tipoCampania)->whereYear('Fecha_Venta', $request->anio)->whereMonth('Fecha_Venta', $request->mes)->where('PBV', 1)->count();
		$ventaimperfecta = Propuestas::where('idCampania', $request->tipoCampania)->whereYear('Fecha_Venta', $request->anio)->whereMonth('Fecha_Venta', $request->mes)->where('PBV', 2)->count();
		$contactados = Propuestas::where('idCampania', $request->tipoCampania)->whereYear('Fecha_Venta', $request->anio)->whereMonth('Fecha_Venta', $request->mes)->whereNotNull('PBV')->count();
		$contactadosOtros = Propuestas::where('idCampania', $request->tipoCampania)->whereYear('Fecha_Venta', $request->anio)->whereMonth('Fecha_Venta', $request->mes)->whereNull('PBV')->orWhereNotIn('PBV', [1, 2])->count();

		//$request->mes;
		$codigos = Codigo::where('idCampania', $request->tipoCampania)->get();

		//$reportes=Propuestas::whereMonth('Fecha_Venta','=',$request->mes)->whereYear('Fecha_Venta','=','2018')->where('idCampania',$request->tipoCampania)->get();
		$sql = "select DAY(Fecha_Venta) as dias,
COUNT(Fecha_Venta) as total,
(select count(id) from propuestas where  MONTH(Fecha_Venta) = $request->mes and YEAR(Fecha_Venta) = $request->anio AND idCampania = $request->tipoCampania and DAY(Fecha_Venta) = dias and PBV in (1) ) as total_dia,
(select count(id) from propuestas where  MONTH(Fecha_Venta) = $request->mes and YEAR(Fecha_Venta) = $request->anio AND idCampania = $request->tipoCampania and DAY(Fecha_Venta) = dias and PBV in (2,11) ) as ventaimperfecta,
((select count(id) from propuestas where  MONTH(Fecha_Venta) = $request->mes and YEAR(Fecha_Venta) = $request->anio AND idCampania = $request->tipoCampania and DAY(Fecha_Venta) = dias and PBV in (1,2,11)  )/count(Fecha_Venta))*100 as promedio
from propuestas where MONTH(Fecha_Venta) = $request->mes and YEAR(Fecha_Venta) = $request->anio AND idCampania =  $request->tipoCampania GROUP BY DAY(Fecha_Venta)";
		$dataGrafica = DB::select($sql);

		$dataReporte = $dataGrafica;
		$grafico = json_encode($dataGrafica);

		//dd($grafico);
		return view('reportes.reporte', ['ano' => $request->anio,
			'mes' => $request->mes,
			'tipoCampania' => $request->tipoCampania,
			'campania' => Campania::pluck('nombre', 'idCampania')->toArray(),
			'codigos' => $codigos,
			'grafico' => $grafico,
			'dataReporte' => $dataReporte,
			'url' => 'reportes/generar',
			'total' => $total,
			'buenaventa' => $buenaVenta,
			'ventaimperfecta' => $ventaimperfecta,
			'contactados' => $contactados,
			'otros' => $contactadosOtros]
		);
	}

	/*generar excel*/
	public function generarExcel(Request $request) {

		//$request->mes;
		$codigos = Codigo::where('idCampania', $request->tipoCampania)->get();

		//$reportes=Propuestas::whereMonth('Fecha_Venta','=',$request->mes)->whereYear('Fecha_Venta','=','2018')->where('idCampania',$request->tipoCampania)->get();
		$sql = "select DAY(Fecha_Venta) as dias,
COUNT(Fecha_Venta) as total,
(select count(id) from propuestas where  MONTH(Fecha_Venta) = $request->mes and YEAR(Fecha_Venta) = $request->anio AND idCampania = $request->tipoCampania and DAY(Fecha_Venta) = dias and PBV in (1) ) as total_dia,
(select count(id) from propuestas where  MONTH(Fecha_Venta) = $request->mes and YEAR(Fecha_Venta) = $request->anio AND idCampania = $request->tipoCampania and DAY(Fecha_Venta) = dias and PBV in (2,11) ) as ventaimperfecta,
((select count(id) from propuestas where  MONTH(Fecha_Venta) = $request->mes and YEAR(Fecha_Venta) = $request->anio AND idCampania = $request->tipoCampania and DAY(Fecha_Venta) = dias and PBV in (1,2,11)  )/count(Fecha_Venta))*100 as promedio
from propuestas where MONTH(Fecha_Venta) = $request->mes and YEAR(Fecha_Venta) = $request->anio AND idCampania =  $request->tipoCampania GROUP BY DAY(Fecha_Venta)";
		$dataGrafica = DB::select($sql);

		$this->mes = $request->mes;
		$this->anio = $request->anio;
		$this->idCampania = $request->tipoCampania;

		\Excel::create('Reporte gestion Excel-'.date('Ymdhis'), function ($excel) {

			$excel->sheet('Productos', function ($sheet) {

				$dataExcel = Propuestas::select('No_Propuesta', 'Poliza','Telefono',
					'RUT',
					'Contratante',
					'Fecha_Venta',
					'Producto',
					'DscPlan',
					//'PBV',
					'Fecha_Llamada',
					'Conecta',
					'No_Conecta',
					'Fecha_Base',
					'Registro',
					'Observaciones',
					'Observaciones2',
					'Motivo_Rechazo',
					'Mes',
					'Ano',
					'Canal_Venta',
					'Rut_Vendedor',
					'Vendedor',
					'Sucursal_Vendedor',
					'Territorial_Vendedor',
					'Prima',
					'propuestas.email',
					'actualizaInformativo',
					'Patente',
					'Modelo',
					'Marca',
					'Segmento',
					'Sub_segmento as linea_Prod',
					'Asesor_Corner as multip',
					'Tipo_inspeccion',
					'Tipo_contrato',
					'AnoAuto',
					'preg_1 as opcion1',
					'recibido',
					'companiaseguro', 'emp_id', 
					'users.name as Nombre_Ejecutivo', 
					'propuestas.idCampania',
					'ambiente',
					'recibido',
					'P1 as confirma_contratacion',
					'poliza_recepcion',
					'P2 as pregunta1',
					'P3 as pregunta2',
					'P4 as pregunta3',
					'P5 as pregunta4'
					)
					->join('users', 'emp_id', '=', 'users.id')
					->selectRaw(DB::raw(" CASE PBV
                    WHEN  1 THEN 'Buena Venta'
                    WHEN 2 THEN 'Venta Imperfecta'
                        WHEN 3  THEN 'Venta Forzada'
                        WHEN 4  THEN 'Equivocado'
                        WHEN 5  THEN 'Buzon de Voz'
                        WHEN 6  THEN 'Fuera de Servicio'
                        WHEN 7  THEN 'Rechaza Bienvenida'
                        WHEN 8  THEN 'No Conectado'
                        WHEN 9  THEN 'No Contesta'
                        WHEN 10 THEN 'Numero malo'
                        WHEN 11 THEN 'Venta Imperfecta'
                        WHEN 12 THEN 'Volver a Llamar'
                        WHEN 11 THEN 'Venta Imperfecta'
                        WHEN 12 THEN 'Volver a Llamar'
                        WHEN 13 THEN 'Venta Imperfecta'
                        WHEN 14 THEN 'Volver a Llamar'
                        WHEN 15 THEN 'No Contactado'
                    ELSE PBV  END  as Estado_Gestion_PBV"))
					->whereMonth('Fecha_Venta', '=', $this->mes)
					->whereYear('Fecha_Venta', '=', $this->anio)
					->where('.propuestas.idCampania', $this->idCampania)->get();
				foreach ($dataExcel as $key => $rs) {
					$dataExcel[$key]['Observaciones'] = html_entity_decode(strip_tags($dataExcel[$key]['Observaciones']));
				}
				$sheet->fromArray($dataExcel);

			});
		})->export('xlsx');

		$grafico = json_encode($dataGrafica);
		//dd($grafico);
		return view('reportes.reporte', ['ano' => $request->anio,
			'mes' => $request->mes,
			'tipoCampania' => $request->tipoCampania,
			'campania' => Campania::pluck('nombre', 'idCampania')->toArray(),
			'codigos' => $codigos,
			'grafico' => $grafico,
			'url' => 'reportes/generar']);
	}

	/*generar excel*/
	public function generarExcelIAXIS($campania,$fecha) {

		//$request->mes;

		$codigos = Codigo::where('idCampania', $campania)->get();

		//$this->anio = $request->anio;
		$this->idCampania = $campania;
		$this->mes=$fecha;

		\Excel::create('Excel IAXIS Gestion Excel-'.date('Ymdhis'), function ($excel) {

			$excel->sheet('Productos', function ($sheet) {

				$dataExcel = Propuestas::select('No_Propuesta', 'Poliza','Telefono',
					'RUT',
					'Contratante',
					'Fecha_Venta',
					'Producto',
					'DscPlan',
					//'PBV',
					'Fecha_Llamada',
					'Conecta',
					'No_Conecta',
					'Fecha_Base',
					'Registro',
					'Observaciones',
					'Observaciones2',
					'Motivo_Rechazo',
					'Mes',
					'Ano',
					'Canal_Venta',
					'Rut_Vendedor',
					'Vendedor',
					'Sucursal_Vendedor',
					'Territorial_Vendedor',
					'Prima',
					'propuestas.email',
					'actualizaInformativo',
					'Patente',
					'Modelo',
					'Marca',
					'Segmento',
					'Sub_segmento as linea_Prod',
					'Asesor_Corner as multip',
					'Tipo_inspeccion',
					'Tipo_contrato',
					'AnoAuto',
					'preg_1 as opcion1',
					'recibido',
					'P1 as confirma_contratacion',
					'poliza_recepcion',
					'P2 as pregunta1',
					'P3 as pregunta2',
					'P4 as pregunta3',
					'P5 as pregunta4',
					'companiaseguro', 'emp_id', 'users.name as Nombre_Ejecutivo', 'propuestas.idCampania')->join('users', 'emp_id', '=', 'users.id')
					->selectRaw(DB::raw(" CASE PBV
                    WHEN  1 THEN 'Buena Venta'
                    WHEN 2 THEN 'Venta Imperfecta'
                        WHEN 3  THEN 'Venta Forzada'
                        WHEN 4  THEN 'Equivocado'
                        WHEN 5  THEN 'Buzon de Voz'
                        WHEN 6  THEN 'Fuera de Servicio'
                        WHEN 7  THEN 'Rechaza Bienvenida'
                        WHEN 8  THEN 'No Conectado'
                        WHEN 9  THEN 'No Contesta'
                        WHEN 10 THEN 'Numero malo'
                        WHEN 11 THEN 'Venta Imperfecta'
                        WHEN 12 THEN 'Volver a Llamar'
                        WHEN 11 THEN 'Venta Imperfecta'
                        WHEN 12 THEN 'Volver a Llamar'
                        WHEN 13 THEN 'Venta Imperfecta'
                        WHEN 14 THEN 'Volver a Llamar'
                        WHEN 15 THEN 'No Contactado'
                    ELSE PBV  END  as Estado_Gestion_PBV"))
					->whereDate('Fecha_Llamada', '=', $this->mes)
					->where('.propuestas.idCampania', $this->idCampania)
					->where('ambiente','IAXIS')
					->get();
					//dd($dataExcel,$this->mes,$this->idCampania);
					
				foreach ($dataExcel as $key => $rs) {
					$dataExcel[$key]['Observaciones'] = html_entity_decode(strip_tags($dataExcel[$key]['Observaciones']));
				}
				$sheet->fromArray($dataExcel);

			});
		})->export('xlsx');

		$grafico = json_encode($dataGrafica);
		//dd($grafico);
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

	public function agentes() {
		//
		session(['active_menu' => 'reportes']);
		$campania = Campania::pluck('nombre', 'idCampania')->toArray();
		//dd($campania);
		return view('reportes.agentes', ['campania' => $campania]);
	}

	public function agentesGenerar() {
		//
		$campania = Campania::pluck('nombre', 'idCampania')->toArray();
		//dd($campania);
		return view('reportes.reporteAgentes', ['campania' => $campania]);
	}
}
