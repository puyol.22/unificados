<?php

namespace App\Http\Controllers;
use App\Campania;
use App\Propuestas;
use App\Tipousuario;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PropuestasController extends Controller {
	public function __construct() {
		$this->middleware('auth');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		session(['active_menu' => 'propuestas']);
		//dd(Auth::user()->idcampania);

		$propuesta = Propuestas::where('propuestas.idCampania', Auth::user()->idcampania)
			->select('propuestas.*', 'users.name as nombre_ejecutivo')
			->join('users', 'users.id', '=', 'propuestas.emp_id')
			->where(function ($query) {
				$query->where('Mes', date('n'))
					->orWhere('Mes', (date('n') - 1));
			})
			->where('Registro', 'Abierto')
			->where('Ano', date('Y'))
			->where('emp_id', Auth::id())->inRandomOrder()
			->first();

		if (date('n') == 1 && $propuesta == null) {
			$propuesta = Propuestas::where('idCampania', Auth::user()->idcampania)
				->where(function ($query) {
					$query->where('Mes', 12);
				})
				->where('Registro', 'Abierto')
				->where('Ano', date('Y') - 1)
				->where('emp_id', Auth::id())->inRandomOrder()
				->first();
		}

		if ($propuesta) {

			$campania = Campania::find($propuesta->idCampania);

			$arrayPropuesta = $propuesta->toArray();
			$search = array_keys($arrayPropuesta);

			$replace = array_values($arrayPropuesta);
			# Our subject is the same as our first example.
			$subject = $campania->script;
			unset($search[0]);
			unset($replace[0]);

			$script = str_replace($search, $replace, $subject);
			$fechaVentaAux = Carbon::parse($propuesta->Fecha_Venta)->format('Y/m/d');
			$sql = "select (count(id)/(select count(id) from propuestas where date(Fecha_Venta) = '$fechaVentaAux'))*100 as promedio  from propuestas where date(Fecha_Venta) = '$fechaVentaAux' and PBV =1";

			$porcentajeCompletados = DB::select($sql);

			if ($propuesta->idCampania != 1) {
				return view('propuestas.show', ["script" => $script, 'propuesta' => $propuesta, "PorcRecorrido" => $porcentajeCompletados[0]->promedio, "FechaVenta" => '']);
			} else {
				return view('propuestas.showExp', ["script" => $script, 'propuesta' => $propuesta, "PorcRecorrido" => $porcentajeCompletados[0]->promedio, "FechaVenta" => '']);
			}
		} else {
			return view('propuestas.find', ['mensaje' => 'No Existe Registro Para Trabajar -- ']);
		}
	}

	public function propuestaSig($id, $fechaVenta = '') {
		$fechaVenta = $fechaVenta;
		session(['active_menu' => 'propuestas']);
		//
		$propuestaId = $id + 1;

		if ($fechaVenta == '') {
			$propuesta = Propuestas::
				where(function ($query) {
				$query->where('Mes', date('n'))
					->orWhere('Mes', (date('n') - 1));
			})
				->select('propuestas.*', 'users.name as nombre_ejecutivo')
				->join('users', 'users.id', '=', 'propuestas.emp_id')
				->where('Ano', date('Y'))->where('propuestas.id', $propuestaId)->where('emp_id', Auth::id())
				->orderBy('id','desc')
				->get();

			if (date('n') == 1) {
				$propuesta = Propuestas::
					where(function ($query) {
					$query->where('Mes', 12);
				})
					->select('propuestas.*', 'users.name as nombre_ejecutivo')
					->join('users', 'users.id', '=', 'propuestas.emp_id')
					->where('Ano', date('Y') - 1)->where('propuestas.id', $propuestaId)->where('emp_id', Auth::id())
					->orderBy('id','desc')
					->get();
			}

		} else {
			$propuesta = Propuestas::whereDate('Fecha_Venta', $fechaVenta)
				->select('propuestas.*', 'users.name as nombre_ejecutivo')
				->join('users', 'users.id', '=', 'propuestas.emp_id')
				->where('id', '>', $id)->where('emp_id', Auth::id())->take(1)
				->orderBy('id','desc')
				->get();
		}

		if ($propuesta->count() > 0) {

			$campania = Campania::find($propuesta[0]->idCampania);

			$arrayPropuesta = $propuesta[0]->toArray();

			$search = array_keys($arrayPropuesta);

			$replace = array_values($arrayPropuesta);
# Our subject is the same as our first example.
			unset($search[0]);
			unset($replace[0]);

			$subject = $campania->script;
			$script = str_replace($search, $replace, $subject);
			$fechaVentaAux = Carbon::parse($propuesta[0]->Fecha_Venta)->format('Y/m/d');
			$sql = "select (count(id)/(select count(id) from propuestas where date(Fecha_Venta) = '$fechaVentaAux'))*100 as promedio  from propuestas where date(Fecha_Venta) = '$fechaVentaAux' and PBV =1";

			$porcentajeCompletados = DB::select($sql);

			return view('propuestas.show', ["script" => $script, 'propuesta' => $propuesta[0], "PorcRecorrido" => $porcentajeCompletados[0]->promedio, "FechaVenta" => $fechaVenta]);
		} else {
			return view('errors.custom', ['mensaje' => 'No Existe Registro Siguente a ' . $id]);
		}

	}

	public function propuestaAnt($id, $fechaVenta = '') {
		$fechaVenta = $fechaVenta;
		session(['active_menu' => 'propuestas']);
		//
		$propuestaId = $id + 1;

		if ($fechaVenta == '') {
			$propuesta = Propuestas::
				where(function ($query) {
				$query->where('Mes', date('n'))
					->orWhere('Mes', (date('n') - 1));
			})
				->select('propuestas.*', 'users.name as nombre_ejecutivo')
				->join('users', 'users.id', '=', 'propuestas.emp_id')
				->where('Ano', date('Y'))->where('propuestas.id', $propuestaId)->where('emp_id', Auth::id())
				->get();

			if (date('n') == 1) {
				$propuesta = Propuestas::
					where(function ($query) {
					$query->where('Mes', 12);
				})
					->select('propuestas.*', 'users.name as nombre_ejecutivo')
					->join('users', 'users.id', '=', 'propuestas.emp_id')
					->where('Ano', date('Y') - 1)->where('propuestas.id', $propuestaId)->where('emp_id', Auth::id())->get();
			}

		} else {
			$propuesta = Propuestas::whereDate('Fecha_Venta', $fechaVenta)
				->select('propuestas.*', 'users.name as nombre_ejecutivo')
				->join('users', 'users.id', '=', 'propuestas.emp_id')
				->where('id', '<', $id)->where('emp_id', Auth::id())->take(1)->get();
		}

		if ($propuesta->count() > 0) {

			$campania = Campania::find($propuesta[0]->idCampania);

			$arrayPropuesta = $propuesta[0]->toArray();

			$search = array_keys($arrayPropuesta);

			$replace = array_values($arrayPropuesta);
# Our subject is the same as our first example.
			unset($search[0]);
			unset($replace[0]);

			$subject = $campania->script;
			$script = str_replace($search, $replace, $subject);
			$fechaVentaAux = Carbon::parse($propuesta[0]->Fecha_Venta)->format('Y/m/d');
			$sql = "select (count(id)/(select count(id) from propuestas where date(Fecha_Venta) = '$fechaVentaAux'))*100 as promedio  from propuestas where date(Fecha_Venta) = '$fechaVentaAux' and PBV =1";

			$porcentajeCompletados = DB::select($sql);

			return view('propuestas.show', ["script" => $script, 'propuesta' => $propuesta[0], "PorcRecorrido" => $porcentajeCompletados[0]->promedio, "FechaVenta" => $fechaVenta]);
		} else {
			return view('errors.custom', ['mensaje' => 'No Existe Registro Siguente a ' . $id]);
		}
	}

	public function propuestaFechaVenta($fechaVenta = '') {
		$fechaVenta = $fechaVenta;

		session(['active_menu' => 'propuestas']);
		//

		if ($fechaVenta != '') {
			$propuesta = Propuestas::whereDate('Fecha_Venta', $fechaVenta)
				->select('propuestas.*', 'users.name as nombre_ejecutivo')
				->join('users', 'users.id', '=', 'propuestas.emp_id')
				->where('emp_id', Auth::id())->get()->first();

		} else {
			$propuesta = Propuestas::where('Mes', date('n'))
				->select('propuestas.*', 'users.name as nombre_ejecutivo')
				->join('users', 'users.id', '=', 'propuestas.emp_id')
				->where('idCampania', Auth::user()->idcampania)
				->where('Ano', date('Y'))
				->where('emp_id', Auth::id())->inRandomOrder()
				->first();

		}

		if ($propuesta) {
			$campania = Campania::find($propuesta->idCampania);

			$arrayPropuesta = $propuesta->toArray();
			$search = array_keys($arrayPropuesta);

			$replace = array_values($arrayPropuesta);
# Our subject is the same as our first example.
			unset($search[0]);
			unset($replace[0]);
			$subject = $campania->script;
			$script = str_replace($search, $replace, $subject);

			$fechaVentaAux = Carbon::parse($propuesta->Fecha_Venta)->format('Y/m/d');
			$sql = "select (count(id)/(select count(id) from propuestas where date(Fecha_Venta) = '$fechaVentaAux'))*100 as promedio  from propuestas where date(Fecha_Venta) = '$fechaVentaAux' and PBV =1";

			$porcentajeCompletados = DB::select($sql);

			if ($propuesta->idCampania != 1) {
				return view('propuestas.show', ["script" => $script, 'propuesta' => $propuesta, "PorcRecorrido" => $porcentajeCompletados[0]->promedio, "FechaVenta" => '']);
			} else {
				return view('propuestas.showExp', ["script" => $script, 'propuesta' => $propuesta, "PorcRecorrido" => $porcentajeCompletados[0]->promedio, "FechaVenta" => '']);
			}
		} else {
			return view('errors.custom', ['mensaje' => 'No Existe Registro ']);
		}
	}

	public function propuestaBuscar(Request $request) {
		session(['active_menu' => 'propuestas']);
		$propuestaId = $request->txtBuscar;
		$propuesta = Propuestas::
			where('emp_id', Auth::id())
			->where(function ($query) {
				$query->where('Mes', date('n'))
					->orWhere('Mes', date('n') - 1);
			})
			->select('propuestas.*', 'users.name as nombre_ejecutivo')
			->join('users', 'users.id', '=', 'propuestas.emp_id')
			->where('No_Propuesta', $propuestaId)
			->where('emp_id', Auth::id())
			->orderBy('id','desc')
			->get();
		//dd($propuesta->count());
		if (date('n') == 1 && $propuesta->count() == 0) {
			//dd($propuesta);
			$propuesta = Propuestas::
				where('emp_id', Auth::id())
				->where(function ($query) {
					$query->where('Mes', 12);
				})
				->select('propuestas.*', 'users.name as nombre_ejecutivo')
				->join('users', 'users.id', '=', 'propuestas.emp_id')
				->where('Ano', date('Y') - 1)
				->where('No_Propuesta', $propuestaId)
				->where('emp_id', Auth::id())
				->orderBy('id','desc')
				->get();
		}

		if ($propuesta->count() >= 1) {

			$campania = Campania::find($propuesta[0]->idCampania);

			$arrayPropuesta = $propuesta[0]->toArray();

			$search = array_keys($arrayPropuesta);

			$replace = array_values($arrayPropuesta);
# Our subject is the same as our first example.
			$subject = $campania->script;

			unset($search[0]);
			unset($replace[0]);

			$script = str_replace($search, $replace, $subject);

			$fechaVentaAux = Carbon::parse($propuesta[0]->Fecha_Venta)->format('Y/m/d');
			$sql = "select (count(id)/(select count(id) from propuestas where date(Fecha_Venta) = '$fechaVentaAux'))*100 as promedio  from propuestas where date(Fecha_Venta) = '$fechaVentaAux' and PBV =1";

			$porcentajeCompletados = DB::select($sql);

			if ($propuesta[0]->idCampania != 1) {
				return view('propuestas.show', ["script" => $script, 'propuesta' => $propuesta[0], "PorcRecorrido" => $porcentajeCompletados[0]->promedio, "FechaVenta" => '']);

			} else {
				return view('propuestas.showExp', ["script" => $script, 'propuesta' => $propuesta[0], "PorcRecorrido" => $porcentajeCompletados[0]->promedio, "FechaVenta" => '']);

			}

		} else {
			return view('errors.custom', ['mensaje' => 'No Existe Registro Anterior a ' . $propuestaId]);
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Responsex|
	 */
	public function update(Request $request, $id) {
		//
		//dd($request);
		$propuesta = Propuestas::find($id);
		
		if($propuesta->idCampania ==2){
			$propuesta->P1=$request->conn;
			if($request->conn == "reconoce"){
				$propuesta->P2=$request->preg1;
			}
		}else{
			$propuesta->P1=$request->conn;
			if($request->conn == "reconoce"){
				$propuesta->P2=$request->preg1;
				if($propuesta->ambiente == "IAXIS"){
					$propuesta->P3=$request->preg2;
					$propuesta->P4=$request->preg3;
					$propuesta->P5=$request->preg4;
					$propuesta->poliza_recepcion=$request->poliza_recepcion;
				}
			}
		}

		if ($propuesta->Registro == "Abierto") {
			if ($request->Gestion == 1) {
				$propuesta->Registro = "Cerrado";
			} else {
				$propuesta->Registro = "Abierto";
			}

			$propuesta->PBV = $request->Gestion;
			$propuesta->email = $request->email;
			//$propuesta->actualizaInformativo = $request->actualizaInformativo;
			$propuesta->Observaciones = $request->editor1;
			$propuesta->Observaciones2 = $request->obsencuesta;
			$propuesta->Fecha_Llamada = date('Y-m-d h:i:s');
			$propuesta->Motivo_Rechazo = $request->Motivo_Rechazo;

			$t1 = $this->parsePhone($request->Telefono_CRM_1);
			$t2 = $this->parsePhone($request->Telefono_CRM_2);
			$t3 = $this->parsePhone($request->Telefono_CRM_3);
			$t4 = $this->parsePhone($request->Telefono_CRM_4);
			$t5 = $this->parsePhone($request->Telefono_CRM_5);

			if (isset($request->preg1)) {
				//$propuesta->preg_1 = $request->preg1;
			}
			$propuesta->Telefono_CRM_1 = $t1[1];
			$propuesta->NoConecta_Tel1 = "$request->NoConecta_Tel1";
			$propuesta->Telefono_CRM_2 = $t2[1];
			$propuesta->NoConecta_Tel2 = "$request->NoConecta_Tel2";
			$propuesta->Telefono_CRM_3 = $t3[1];
			$propuesta->NoConecta_Tel3 = "$request->NoConecta_Tel3";
			$propuesta->Telefono_CRM_4 = $t4[1];
			$propuesta->NoConecta_Tel4 = "$request->NoConecta_Tel4";
			$propuesta->Telefono_CRM_5 = $t5[1];
			$propuesta->NoConecta_Tel5 = "$request->NoConecta_Tel5";

		} elseif ($request->Gestion == 1 && $propuesta->Registro == "Cerrado") {

			$propuesta->PBV = $request->Gestion;
			//$propuesta->Conecta = $request->Conecta;
			//$propuesta->No_Conecta = $request->No_Conecta;
			$propuesta->email = $request->email;
			//$propuesta->actualizaInformativo = $request->actualizaInformativo;
			$propuesta->Observaciones = $request->editor1;
			$propuesta->Observaciones2 = $request->obsencuesta;
			$propuesta->Motivo_Rechazo = $request->Motivo_Rechazo;

			$t1 = $this->parsePhone($request->Telefono_CRM_1);
			$t2 = $this->parsePhone($request->Telefono_CRM_2);
			$t3 = $this->parsePhone($request->Telefono_CRM_3);
			$t4 = $this->parsePhone($request->Telefono_CRM_4);
			$t5 = $this->parsePhone($request->Telefono_CRM_5);

			$propuesta->Telefono_CRM_1 = $t1[1];
			$propuesta->NoConecta_Tel1 = "$request->NoConecta_Tel1";
			$propuesta->Telefono_CRM_2 = $t2[1];
			$propuesta->NoConecta_Tel2 = "$request->NoConecta_Tel2";
			$propuesta->Telefono_CRM_3 = $t3[1];
			$propuesta->NoConecta_Tel3 = "$request->NoConecta_Tel3";
			$propuesta->Telefono_CRM_4 = $t4[1];
			$propuesta->NoConecta_Tel4 = "$request->NoConecta_Tel4";
			$propuesta->Telefono_CRM_5 = $t5[1];
			$propuesta->NoConecta_Tel5 = "$request->NoConecta_Tel5";
		} else {

			$propuesta->Registro = "Abierto";
			$propuesta->PBV = $request->Gestion;
			//$propuesta->Conecta = $request->Conecta;
			//$propuesta->No_Conecta = $request->No_Conecta;
			$propuesta->email = $request->email;

			//$propuesta->actualizaInformativo = $request->actualizaInformativo;
			$propuesta->Observaciones = $request->editor1;
			$propuesta->Observaciones2 = $request->obsencuesta;
			$propuesta->Fecha_Llamada = date('Y-m-d h:i:s');
			// $propuesta->PBV->=$request->Gestion;
			$propuesta->Motivo_Rechazo = $request->Motivo_Rechazo;
			if (isset($request->preg1)) {
				//$propuesta->preg_1 = $request->preg1;;
			}

			$t1 = $this->parsePhone($request->Telefono_CRM_1);
			$t2 = $this->parsePhone($request->Telefono_CRM_2);
			$t3 = $this->parsePhone($request->Telefono_CRM_3);
			$t4 = $this->parsePhone($request->Telefono_CRM_4);
			$t5 = $this->parsePhone($request->Telefono_CRM_5);

			$propuesta->Telefono_CRM_1 = $t1[1];
			$propuesta->NoConecta_Tel1 = "$request->NoConecta_Tel1";
			$propuesta->Telefono_CRM_2 = $t2[1];
			$propuesta->NoConecta_Tel2 = "$request->NoConecta_Tel2";
			$propuesta->Telefono_CRM_3 = $t3[1];
			$propuesta->NoConecta_Tel3 = "$request->NoConecta_Tel3";
			$propuesta->Telefono_CRM_4 = $t4[1];
			$propuesta->NoConecta_Tel4 = "$request->NoConecta_Tel4";
			$propuesta->Telefono_CRM_5 = $t5[1];
			$propuesta->NoConecta_Tel5 = "$request->NoConecta_Tel5";
		}



		$saved = $propuesta->save();
		if ($saved) {
			return redirect('propuestas');
		} else {
			return view('errors.custom', ['mensaje' => 'Error al guardar registro']);
		}
	}

	public function parsePhone($array) {
		$array = explode('-', $array);
		return array($array[0], $array[count($array) - 1]);

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

	public function buscarPropuestas(Request $request) {

		//session(['active_menu' => 'propuestas']);
		//
		$propuestaId = $request->propuesta;

		$propuesta = Propuestas::where('No_Propuesta', $propuestaId)->orderBy('id','desc')->take(1)->get();

		if ($propuesta->count() > 0) {

			$campania = Campania::find($propuesta[0]->idCampania);

			$arrayPropuesta = $propuesta[0]->toArray();

			$search = array_keys($arrayPropuesta);

			$replace = array_values($arrayPropuesta);
			unset($search[0]);
			unset($replace[0]);

			$subject = $campania->script;
			$script = str_replace($search, $replace, $subject);

			return view('propuestas.showAdmin', ["script" => $script, 'propuesta' => $propuesta[0], "PorcRecorrido" => 0, "FechaVenta" => 0]);
		} else {
			return view('errors.custom', ['mensaje' => 'No Existe Registro Siguente ']);
		}
	}

	public function inicioBuscar() {
		return view('propuestas.buscar', ['mensaje' => 'Buscar propuestas']);
	}

	public function updateAdmin(Request $request, $id) {
		//
		//dd($request);

		$idUsuario = Auth::user()->idtipousuario;
		$accesoID = Tipousuario::find($idUsuario);
		if ($accesoID->id == 1 or $accesoID->id == 2) {
			$propuesta = Propuestas::find($id);
			$propuesta->PBV = $request->Gestion;
			//$propuesta->Conecta = $request->Conecta;
			$propuesta->No_Conecta = $request->No_Conecta;
			$propuesta->email = $request->email;
			$propuesta->Registro = $request->Registro;
			//$propuesta->actualizaInformativo = $request->actualizaInformativo;
			$propuesta->Observaciones = $request->editor1;
			$propuesta->Observaciones2 = $request->obsencuesta;
			$propuesta->Fecha_Llamada = $request->ultima_llamada;
			// $propuesta->PBV->=$request->Gestion;
			$propuesta->Motivo_Rechazo = $request->Motivo_Rechazo;
			$t1 = $this->parsePhone($request->Telefono_CRM_1);
			$t2 = $this->parsePhone($request->Telefono_CRM_2);
			$t3 = $this->parsePhone($request->Telefono_CRM_3);
			$t4 = $this->parsePhone($request->Telefono_CRM_4);
			$t5 = $this->parsePhone($request->Telefono_CRM_5);

			$propuesta->Telefono_CRM_1 = $t1[1];
			$propuesta->NoConecta_Tel1 = "$request->NoConecta_Tel1";
			$propuesta->Telefono_CRM_2 = $t2[1];
			$propuesta->NoConecta_Tel2 = "$request->NoConecta_Tel2";
			$propuesta->Telefono_CRM_3 = $t3[1];
			$propuesta->NoConecta_Tel3 = "$request->NoConecta_Tel3";
			$propuesta->Telefono_CRM_4 = $t4[1];
			$propuesta->NoConecta_Tel4 = "$request->NoConecta_Tel4";
			$propuesta->Telefono_CRM_5 = $t5[1];
			$propuesta->NoConecta_Tel5 = "$request->NoConecta_Tel5";

			$saved = $propuesta->save();
			if ($saved) {
				return view('propuestas.buscar');
			} else {
				return view('errors.custom', ['mensaje' => 'Error al guardar registro']);
			}
		} else {
			return view('errors.custom', ['mensaje' => 'Sin permisos de ejecucioón']);
		}
	}
}
