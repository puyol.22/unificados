<?php

namespace App\Http\Controllers;

use App\Campania;
use App\Codigo;
use Illuminate\Http\Request;

class CodigoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('mantenedores.codigo',['codigo'=>Codigo::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('mantenedores.codigo_nuevo',['campania'=>Campania::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $codigo = new Codigo();
        $codigo->producto=$request->producto;
        $codigo->codigo=$request->codigo;
        $codigo->valor=$request->valor;
        $codigo->estado=$request->estado;
        $codigo->tipo=$request->tipo;
        $codigo->idcampania=$request->campania;
        $saved=$codigo->save();
        if($saved){
            return view('mantenedores.codigo',['codigo'=>Codigo::all()]);
        }else{
            return view('errors.custom', ['mensaje' => 'ERROR AL CREAR CODIGO']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return view('mantenedores.codigo_editar',['codigo' => Codigo::find($id),'campania'=>Campania::all()]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $codigo = Codigo::find($id);
        $codigo->producto=$request->producto;
        $codigo->codigo=$request->codigo;
        $codigo->valor=$request->valor;
        $codigo->estado=$request->estado;
        $codigo->tipo=$request->tipo;
        $codigo->idcampania=$request->campania;
        $saved=$codigo->save();
        if($saved){
            return view('mantenedores.codigo',['codigo'=>Codigo::all()]);
        }else{
            return view('errors.custom', ['mensaje' => 'ERROR AL CREAR CODIGO']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
