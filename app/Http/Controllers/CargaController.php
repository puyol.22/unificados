<?php

namespace App\Http\Controllers;

use App\Campania;
use App\Carga;
use App\Propuestas;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CargaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        session(['active_menu' => 'carga']);

        return view('carga.carga',['carga' => Carga::orderBy('id', 'desc')->paginate(5),'campania'=>Campania::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{


        //
        $carga= new Carga();
        $propuestas =  new Propuestas();
        $file = $request->file('Exel');
        //Display File Name


        //Display File Extension
        //echo 'File Extension: '.$file->getClientOriginalExtension();


        //Display File Real Path
        //echo 'File Real Path: '.$file->getRealPath();


        //Display File Size
        //echo 'File Size: '.$file->getSize();
        //echo '<br>';

        //Display File Mime Type
        //echo 'File Mime Type: '.$file->getMimeType();

        //Move Uploaded File
        $destinationPath = 'uploads';
        $file->move($destinationPath,$file->getClientOriginalName());
         //leer excel
        $patchExcel=$destinationPath.'/'.$file->getClientOriginalName();

        if($request->hasFile('Exel')) {
            //$path = Input::file($patchExcel)->getRealPath();
            $data = Excel::load($patchExcel, function ($reader) {
            })->get();

            $contadorR=0;
            $arrayInsert=array();
            if (!empty($data) && $data->count()) {
                foreach ($data->toArray() as $row) {
                    //dd($row);
                    //$insert[] = ['title' => $value->title, 'description' => $value->descriptionulln];
                    if(isset($row["no_propuesta"])){
                    $arrayInsert[] = ['id_propuesta' => isset($row['id_propuesta']) ? $row['id_propuesta'] : 0,
                        'no_propuesta' => isset($row["no_propuesta"]) ? $row["no_propuesta"] : rand(90000000, 99999999),
                        'poliza' => isset($row["poliza"]) ? $row["poliza"] : null,
                        'telefono' => isset($row["telefono"]) ? $row["telefono"] : null,
                        'rut' => isset($row["rut"]) ? $row["rut"] : null,
                        'Contratante' => isset($row["contratante"]) ? $row["contratante"] : null,
                        'fecha_venta' => isset($row["fecha_venta"]) ? $row["fecha_venta"] : null,
                        'fecha_ingreso' => isset($row["fecha_ingreso"]) ? $row["fecha_ingreso"] : null,
                        'producto' => isset($row["producto"]) ? $row["producto"] : null,
                        'dscplan' => isset($row["dscplan"]) ? $row["dscplan"] : null,
                        'codigo_producto' => isset($row["codigo_producto"]) ? $row["codigo_producto"] : null,
                        'id_grabacion' => isset($row["id_grabacion"]) ? $row["id_grabacion"] : null,
                        'asesor_corner' => isset($row["asesor_corner"]) ? $row["asesor_corner"] : null,
                        'direccion_corner_territorial' => isset($row["direccion_corner_territorial"]) ? $row["direccion_corner_territorial"] : null,
                        'auto' => isset($row["auto"]) ? $row["auto"] : null,
                        'pbv' => isset($row["pbv"]) ? $row["pbv"] : null,
                        'fecha_llamada' => isset($row["fecha_llamada"]) ? $row["fecha_llamada"] : null,
                        'conecta' => isset($row["conecta"]) ? $row["conecta"] : null,
                        "no_conecta" => isset($row["no_conecta"]) ? $row["no_conecta"] : null,
                        'fecha_base' => isset($row["fecha_base"]) ? $row["fecha_base"] : date('Y-m-d H:i:s'),
                        //'registro'=>isset($row["registro"])?$row["registro"]:null,
                        'observaciones' => isset($row["observaciones"]) ? $row["observaciones"] : null,
                        'sucursal' => isset($row["sucursal"]) ? $row["sucursal"] : null,
                        'motivo_rechazo' => isset($row["motivo_rechazo"]) ? $row["motivo_rechazo"] : null,
                        'emp_id' => isset($row["emp_id"]) ? $row["emp_id"] : 0,
                        'tipo_telefono_1' => isset($row["tipo_telefono_1"]) ? $row["tipo_telefono_1"] : null,
                        'area_1' => isset($row["area_1"]) ? $row["area_1"] : null,
                        'telefono_crm_1' => isset($row["telefono_crm_1"]) ? $row["telefono_crm_1"] : null,
                        'tipo_telefono_2' => isset($row["tipo_telefono_2"]) ? $row["tipo_telefono_2"] : null,
                        'area_2' => isset($row["area_2"]) ? $row["area_2"] : null,
                        'telefono_crm_2' => isset($row["telefono_crm_2"]) ? $row["telefono_crm_2"] : null,
                        'tipo_telefono_3' => isset($row["tipo_telefono_3"]) ? $row["tipo_telefono_3"] : null,
                        'area_3' => isset($row["area_3"]) ? $row["area_3"] : null,
                        'telefono_crm_3' => isset($row["telefono_crm_3"]) ? $row["telefono_crm_3"] : null,
                        'tipo_telefono_4' => isset($row["tipo_telefono_4"]) ? $row["tipo_telefono_4"] : null,
                        'area_4' => isset($row["area_4"]) ? $row["area_4"] : null,
                        'telefono_crm_4' => isset($row["telefono_crm_4"]) ? $row["telefono_crm_4"] : null,
                        'tipo_telefono_5' => isset($row["tipo_telefono_5"]) ? $row["tipo_telefono_5"] : null,
                        'area_5' => isset($row["area_5"]) ? $row["area_5"] : null,
                        'telefono_crm_5' => isset($row["telefono_crm_5"]) ? $row["telefono_crm_5"] : null,
                        'mes' => isset($request->mes) ? $request->mes : null,
                        'ano' => isset($request->ano) ? $request->ano : null,
                        'noconecta_tel1' => isset($row["noconecta_tel1"]) ? $row["noconecta_tel1"] : null,
                        'noconecta_tel2' => isset($row["noconecta_tel2"]) ? $row["noconecta_tel2"] : null,
                        'noconecta_tel3' => isset($row["noconecta_tel3"]) ? $row["noconecta_tel3"] : null,
                        'noconecta_tel4' => isset($row["noconecta_tel4"]) ? $row["noconecta_tel4"] : null,
                        'noconecta_tel5' => isset($row["noconecta_tel5"]) ? $row["noconecta_tel5"] : null,
                        'canal_venta' => isset($row["canal_venta"]) ? $row["canal_venta"] : null,
                        'rut_vendedor' => isset($row["rut_vendedor"]) ? $row["rut_vendedor"] : null,
                        'vendedor' => isset($row["vendedor"]) ? $row["vendedor"] : null,
                        'sucursal_vendedor' => isset($row["sucursal_vendedor"]) ? $row["sucursal_vendedor"] : null,
                        'territorial_vendedor' => isset($row["territorial_vendedor"]) ? $row["territorial_vendedor"] : null,
                        'zona_vendedor' => isset($row["zona_vendedor"]) ? $row["zona_vendedor"] : null,
                        'si_acepto_seguro' => isset($row["si_acepto_seguro"]) ? $row["si_acepto_seguro"] : null,
                        'prima' => isset($row["prima"]) ? $row["prima"] : null,
                        'p1' => isset($row["p1"]) ? $row["p1"] : null,
                        'p2' => isset($row["p2"]) ? $row["p2"] : null,
                        'email' => isset($row["email"]) ? $row["email"] : null,
                        'p3' => isset($row["p3"]) ? $row["p3"] : null,
                        'actualizainformativo' => isset($row["actualizainformativo"]) ? $row["actualizainformativo"] : null,
                        'updated_at' => date('Y-m-d h:i:s'),
                        'created_at' => date('Y-m-d h:i:s'),
                        'anoAuto' => isset($row['anoauto'])?$row['anoauto']:null,
                        'patente'=>isset($row['patente'])?$row['patente']:null,
                        'modelo'=>isset($row['modelo'])?$row['modelo']:null,
                        'marca'=>isset($row['marca'])?$row['marca']:null,
                        'segmento'=>isset($row['segmento'])?$row['segmento']:null,
                        'sub_segmento'=>isset($row['sub_segmento'])?$row['sub_segmento']:null,
                        'tipo_inspeccion'=>isset($row['tipo_inspeccion'])?$row['tipo_inspeccion']:null,
                        'tipo_contrato'=>isset($row['tipo_contrato'])?$row['tipo_contrato']:null,
						'companiaseguro'=>isset($row['companiaseguro'])?$row['companiaseguro']:null,
                        'idcampania' => isset($request->campania) ? $request->campania : "",
                        'recibido'=>isset($row['recibido'])?$row['recibido']:null,
						'ambiente' => isset($row['ambiente']) ? $row['ambiente'] : ""
                    ];
                    //dd($arrayInsert);

                    $contadorR++;
                    }

                }
                 //dd($arrayInsert);
                //Propuestas::insert($arrayInsert);

                foreach (array_chunk($arrayInsert, 70) as $t) {
                DB::table('propuestas')->insert($t);
                }
                //dd($arrayInsert);
                //exit();

                $carga->nombre=$file->getClientOriginalName();
                $carga->idCampania=$request->idCampania;
                $carga->cantidadRegistro=$contadorR;
                
                //dd($arrayInsert);
                $carga->save();
                return view('carga.carga',['carga' => Carga::orderBy('id', 'desc')->paginate(5),'campania'=>Campania::all()]);
            }
        }else{
            return view('errors.custom', ['mensaje' => 'Archivo con 0 registros']);
        }
        }catch (exception $e){
            return view('errors.custom', ['mensaje' => 'ERROR AL INSERTAR REGISTROS']);
        }




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
