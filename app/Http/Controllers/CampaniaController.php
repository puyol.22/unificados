<?php

namespace App\Http\Controllers;

use App\Campania;
use Illuminate\Http\Request;

class CampaniaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('mantenedores.campania',['campania' => Campania::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('mantenedores.campania_nuevo');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $campania = new Campania();
        $campania->nombre  = $request->name;
        $campania->tabla   = $request->tabla;
        $campania->script  = $request->editor1;
        //$campania->update  = date('Y-m-d');
        //$campania->create  = date('Y-m-d');
        $saved=$campania->save();
        if($saved){
            return view('mantenedores.campania',['campania' => Campania::all()]);
        }else{
            return view('errors.custom', ['mensaje' => 'ERROR AL CREAR CAMPAÑA']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return view('mantenedores.campania_editar',['campania' => Campania::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $campania = Campania::find($id);
        $campania->nombre  = $request->name;
        $campania->tabla   = $request->tabla;
        $campania->script  = $request->editor1;
        //$campania->update  = date('Y-m-d');
        //$campania->create  = date('Y-m-d');
        $saved=$campania->save();
        if($saved){
            return view('mantenedores.campania',['campania' => Campania::all()]);
        }else{
            return view('errors.custom', ['mensaje' => 'ERROR AL ACTUALIZAR CAMPAÑA #:'.$id]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
